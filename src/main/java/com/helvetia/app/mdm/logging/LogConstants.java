package com.helvetia.app.mdm.logging;

public class LogConstants {

    private LogConstants() {
        throw new AssertionError();
    }

    public static final String KEY_APP = "app";
    public static final String KEY_VERSION = "version";
    public static final String KEY_USER_ID = "userId";
}
