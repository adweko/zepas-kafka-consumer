package com.helvetia.app.mdm.logging;

import com.helvetia.app.mdm.config.HelperWsClient;
import generated.MdmFullPerson;
import io.netty.handler.logging.LogLevel;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.MDC;
import statePartner.json.Response.Row;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasIdentificationNumber.hasIdentificationNumberSchema.IdentificationNumber.IdentificationNumberEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isDuplicate.isDuplicateSchema.BusinessPartnerID.BusinessPartnerIDEntry;

import javax.enterprise.context.ApplicationScoped;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Slf4j
@ApplicationScoped
public class LogHelper {

    @ConfigProperty(name = "ch.helvetia.kafka.log.app")
    String app;
    @ConfigProperty(name = "ch.helvetia.kafka.log.version")
    String version;

    public final static String ZEPASPARTNERKEY = "1";
    public final static String SYRIUSPARTNERKEY = "2";

    private void logGeneric(String userId, String trId, ProcessorContext context, String message, LogLevel level) {
        try {
            MDC.put(LogConstants.KEY_APP, app);
            MDC.put(LogConstants.KEY_VERSION, version);
            MDC.put(LogConstants.KEY_USER_ID, userId);

            String outputMessage = ", ";

            if ((trId != null) && (!trId.isEmpty())) {
                outputMessage = outputMessage + "trId=\"" + trId + "\", ";
            }

            outputMessage = outputMessage + getKafkaStatsForSplunk(context);

            if ( ! message.trim().startsWith("message=") ) {
                outputMessage = outputMessage + "message='" + message.replaceAll("'", "\'") + "'";
            }

            if (level == LogLevel.TRACE) {
                log.trace(outputMessage);
            } else if (level == LogLevel.DEBUG) {
                log.debug(outputMessage);
            } else if (level == LogLevel.INFO) {
                log.info(outputMessage);
            } else if (level == LogLevel.WARN) {
                log.warn(outputMessage);
            } else if (level == LogLevel.ERROR) {
                log.error(outputMessage);
            }
        } catch (IllegalThreadStateException e) {
            log.error("logInfo - " + e.getMessage());
        } finally {
            MDC.clear();
        }
    }

    public void logTrace(String userId, String trId, String message) {
        logGeneric(userId, trId, null, message, LogLevel.TRACE);
    }

    public void logTrace(String userId, String trId, ProcessorContext context, String message) {
        logGeneric(userId, trId, context, message, LogLevel.TRACE);
    }

    public void logDebug(String userId, String trId, String message) {
        logGeneric(userId, trId, null, message, LogLevel.DEBUG);
    }

    public void logDebug(String userId, String trId, ProcessorContext context, String message) {
        logGeneric(userId, trId, context, message, LogLevel.DEBUG);
    }

    public void logInfo(String userId, String trId, String message) {
        logGeneric(userId, trId, null, message, LogLevel.INFO);
    }

    public void logInfo(String userId, String trId, ProcessorContext context, String message) {
        logGeneric(userId, trId, context, message, LogLevel.INFO);
    }

    public void logWarn(String userId, String trId, String message) {
        logGeneric(userId, trId, null, message, LogLevel.WARN);
    }

    public void logWarn(String userId, String trId, ProcessorContext context, String message) {
        logGeneric(userId, trId, context, message, LogLevel.WARN);
    }

    public void logError(String userId, String trId, String message) {
        logGeneric(userId, trId, null, message, LogLevel.ERROR);
    }

    public void logError(String userId, String trId, ProcessorContext context, String message) {
        logGeneric(userId, trId, context, message, LogLevel.ERROR);
    }

    public void logError(String trId, Throwable exception) {
        logGeneric(trId, null, exception, LogLevel.ERROR);
    }

    public void logError(String trId, ProcessorContext context, Throwable exception) {
        logGeneric(trId, context, exception, LogLevel.ERROR);
    }

    private void logGeneric(String trId, ProcessorContext context, Throwable exception, LogLevel level) {
        try {
            MDC.put(LogConstants.KEY_APP, app);
            MDC.put(LogConstants.KEY_VERSION, version);
            MDC.put(LogConstants.KEY_USER_ID, "CH_MDM");
            String outputMessage =", ";

            if ((trId != null) && (!trId.isEmpty())) {
                outputMessage = outputMessage + "trId=\"" + trId + "\", ";
            }

            outputMessage = outputMessage + getKafkaStatsForSplunk(context);

            if (level == LogLevel.TRACE) {
                log.trace(outputMessage, exception);
            } else if (level == LogLevel.DEBUG) {
                log.debug(outputMessage, exception);
            } else if (level == LogLevel.INFO) {
                log.info(outputMessage, exception);
            } else if (level == LogLevel.WARN) {
                log.warn(outputMessage, exception);
            } else if (level == LogLevel.ERROR) {
                log.error(outputMessage, exception);
            }
        } catch (IllegalThreadStateException e) {
            log.error("logInfo - " + e.getMessage());
        } finally {
            MDC.clear();
        }
    }

    public static String getTechMdmPartnerID(Row row) {

        String result = "unknown";

        try {

            if (row.getVersions().size() > 0) {
                result = row.getVersions().get(0).getHasIdentificationNumber().get(0).getTECHMDMPartnerID().toString();
            } else if (row.getTECHMDMPartnerID() != null) {
                result = row.getTECHMDMPartnerID().toString();
            }

        } catch (NullPointerException ex) {
            // just for private analytic reasons: in case of an exception: upper case
            result = "UNKNOWN";
        } catch (IndexOutOfBoundsException ex) {
            result = "UnKnown";
        } catch (Exception ex) {
            result = "UnKnowN";
        }

        return result;
    }

    // ZEPAS => partnerId: 1
    // Syrius => partnerId: 2
    public static String getPartnerId(Row row, String partnerKey) {

        String result = "unknown";

        try {

            if (row.getVersions().size() > 0) {

                List<IdentificationNumberEntry> identificationNumbers = row.getVersions().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber();
                if (identificationNumbers != null) {

                    int entry = 0;
                    while ((entry < identificationNumbers.size()) && (result.equals("unknown"))) {
                        if (identificationNumbers.get(entry).getIDType().toString().equals(partnerKey)) {
                            result = identificationNumbers.get(entry).getIDNumber().toString();
                        }
                        entry++;
                    }

                }

            }

        } catch (NullPointerException ex) {
            // just for private analytic reasons: in case of an exception: upper case
            result = "UNKNOWN";
        } catch (IndexOutOfBoundsException ex) {
            result = "UnKnown";
        } catch (Exception ex) {
            result = "UnKnowN";
        }

        return result;
    }

    public static String getKafkaStatsForSplunk ( ProcessorContext context ) {
        String result = "";

        if ( context != null ) {
            if ( context.topic() != null) {
                result = result + "topic=\"" + context.topic() + "\", ";
            }
            result = result + "offset=" + context.offset() + ", ";
            result = result + "partition=" + context.partition() + ", ";

        }

        return result;

    }

}
