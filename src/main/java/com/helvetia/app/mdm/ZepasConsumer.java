//package com.helvetia.app.mdm;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.helvetia.app.mdm.config.HelperWsClient;
//import com.helvetia.app.mdm.config.WsConfiguration;
//import com.helvetia.app.mdm.logging.LogHelper;
//import com.helvetia.app.mdm.mapping.LegalPersonMapper;
//import com.helvetia.app.mdm.mapping.NaturalPersonMapper;
//import com.jayway.jsonpath.*;
//import generated.*;
//import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer;
//import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
//import io.confluent.kafka.serializers.KafkaAvroDeserializer;
//import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
//
//import org.apache.avro.Schema;
//import org.apache.kafka.clients.CommonClientConfigs;
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.consumer.ConsumerRecords;
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//import org.apache.kafka.common.config.SslConfigs;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.eclipse.microprofile.config.inject.ConfigProperty;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
//import statePartner.json.readFullPartnerAllVersionsFromDateResponse;
//
//import javax.enterprise.context.ApplicationScoped;
//import javax.inject.Inject;
//import javax.xml.bind.JAXB;
//import javax.xml.datatype.DatatypeConfigurationException;
//import javax.xml.namespace.QName;
//import javax.xml.ws.Holder;
//import java.io.IOException;
//import java.io.StringWriter;
//import java.time.Duration;
//import java.time.Instant;
//import java.util.*;
//
//@ApplicationScoped
//public class ZepasConsumer {
//
//    @ConfigProperty(name = "ch.helvetia.kafka.schema.registry.url")
//    String schemaRegistryUrl;
//
//    @ConfigProperty(name = "ch.helvetia.kafka.bootstrap.servers")
//    String bootstrapServers;
//
//    @ConfigProperty(name = "ch.helvetia.kafka.group.id")
//    String groupID;
//
//    @ConfigProperty(name = "ch.helvetia.kafka.input.topic")
//    String inputTopic;
//
//	@ConfigProperty(name = "ch.helvetia.kafka.auto.offset.reset")
//	String autoOffsetReset;
//
//    @Inject
//    WsConfiguration wsConfiguration;
//
//    @Inject
//    NaturalPersonMapper naturalPersonMapper;
//
//    @Inject
//    LegalPersonMapper legalPersonMapper;
//
//    @Inject
//    LogHelper logHelper;
//
//    DocumentContext context;
//
//    Holder<Long> personenLaufNummer = new Holder<>();
//    Holder<List<Status>> status = new Holder<>();
//    WsControl wsControl = new WsControl();
//
//    Configuration conf = Configuration.builder().options(Option.DEFAULT_PATH_LEAF_TO_NULL).build();
//    MdmFullPersonV50_Service service = new MdmFullPersonV50_Service(ZepasConsumer.class.getResource("/wsdl/MdmFullPerson_v5_0.wsdl"));
//    static final QName PORT_NAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/",
//            "MdmFullPerson_v5_0Port");
//    MdmFullPersonV50 port;
//    Logger logger = LoggerFactory.getLogger(ZepasConsumer.class);
//
//    static final String ZEPAS = "CH_ZEPAS";
//
//    private static String classToJson(Object object) {
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            return mapper.writeValueAsString(object);
//        } catch (JsonProcessingException e) {
//            return "object not serializable";
//        }
//    }
//
//    private static String objectToXml(Object object) {
//        try {
//            StringWriter sw = new StringWriter();
//            JAXB.marshal(object, sw);
//            String result = sw.toString().replace("\n", "").replace("\r", "").replace("\t", "");
//            while (result.contains(" <")) {
//                result = result.replace(" <", "<");
//            }
//            while (result.contains("> ")) {
//                result = result.replace("> ", ">");
//            }
//            return result;
//        } catch (Exception ex) {
//            return "xml not valid:" + ex.getMessage();
//        }
//    }
//
//    public void startConsuming() throws IOException {
//        port = service.getPort(PORT_NAME, MdmFullPersonV50.class);
//        wsControl.setUserId(wsConfiguration.getLoggedInUser());
//        wsControl.setLanguage("de");
//        logger.info("{}", wsConfiguration.getWsUrlMdmFullPerson());
//        wsConfiguration.setup();
//        KafkaConsumer<String, readFullPartnerAllVersionsFromDateResponse> consumer = new KafkaConsumer<>(configure());
//        consumer.subscribe(Arrays.asList(inputTopic));
//
//        // initial - set a UUID (will be overwritten for each message)
//        String logTransactionId = UUID.randomUUID().toString();
//        String logLoggedInUser = wsConfiguration.getLoggedInUser();
//
//        while (true) { //TODO: für Dry-Run Story entfernen
//            try {
//                while (true) {
//                    ConsumerRecords<String, readFullPartnerAllVersionsFromDateResponse> records = consumer.poll(Duration.ofMillis(100));
//                    for (ConsumerRecord<String, readFullPartnerAllVersionsFromDateResponse> record : records) {
//
//                        logTransactionId = UUID.randomUUID().toString();
//                        logTransactionId = LogHelper.getTechMdmPartnerID(record.value().getBusinessPartnerID().get(0))
//                                + "|" + record.offset()
//                                + "|" + LogHelper.getPartnerId(record.value().getBusinessPartnerID().get(0),LogHelper.ZEPASPARTNERKEY)
//                        + "|" + LogHelper.getPartnerId(record.value().getBusinessPartnerID().get(0),LogHelper.SYRIUSPARTNERKEY);
//
//                        HelperWsClient.prepareWs(port, wsConfiguration.getWsUrlMdmFullPerson(), wsConfiguration);
//                        logHelper.logInfo(logLoggedInUser, logTransactionId, "offset: " + record.offset() + " key: " + record.key() + " value: " + record.value());
//                        readFullPartnerAllVersionsFromDateResponse mdmState = record.value();
//                        context = JsonPath.using(conf).parse(record.value());
//                        String sourceSytem = String.valueOf(mdmState.getBusinessPartnerID().get(0).getTECHSourceSystem());
//                        List<VersionsEntry> versions = mdmState.getBusinessPartnerID().get(0).getVersions();
//                        long startTime = System.nanoTime();
//                        VersionsEntry version = versions.get(0);
//                        if (version.getIsNaturalPerson().size() > 0 && !sourceSytem.equals(ZEPAS)) {
//                            MdmFullPerson mdmFullPerson = naturalPersonMapper
//                                    .mapMdmFullPerson(mdmState);
//                            logHelper.logInfo(logLoggedInUser, logTransactionId, "will try to send nat. Person " + objectToXml(mdmFullPerson));
//                            port.synchronize(mdmFullPerson, HelperWsClient.getCalendar(Date.from(Instant.now())), wsControl, personenLaufNummer, status);
//                            // HelperWsClient.printStatus(status.value);
//                            logHelper.logInfo(logLoggedInUser, logTransactionId, "Zepas Response: " + HelperWsClient.getStatus(status.value));
//                        } else if (version.getIsLegalPerson().size() > 0 && !sourceSytem.equals(ZEPAS)) {
//                            if ( version.getHasChannelID().isEmpty() ) {
//                                logHelper.logInfo(logLoggedInUser, logTransactionId, "Ignoring legal Person without channel");
//                            } else {
//                                MdmFullPerson mdmFullPerson = legalPersonMapper.mapMdmFullPerson(version);
//                                logHelper.logInfo(logLoggedInUser, logTransactionId, "will try to send legal Person " + objectToXml(mdmFullPerson));
//                                port.synchronize(mdmFullPerson, HelperWsClient.getCalendar(Date.from(Instant.now())), wsControl, personenLaufNummer, status);
//                                // HelperWsClient.printStatus(status.value);
//                                logHelper.logInfo(logLoggedInUser, logTransactionId, "Zepas Response: " + HelperWsClient.getStatus(status.value));
//                            }
//                        } else {
//                            logHelper.logInfo(logLoggedInUser, logTransactionId, "Message was originally mine");
//                        }
//                        consumer.commitSync();
//                        logHelper.logInfo(logLoggedInUser, logTransactionId,
//                                "Message processing Duration " + ((System.nanoTime() - startTime) / 1000000) + " ms");
//                        logHelper.logInfo(logLoggedInUser, logTransactionId,
//                                "successfully processed");
//                    }
//                }
//            } catch (DatatypeConfigurationException | ZepaswebServiceException_Exception | PathNotFoundException e) {
//                logHelper.logError(logTransactionId, e);
//            } catch (Exception e) {
//                logHelper.logError(logLoggedInUser, logTransactionId, "An unspecific exception occurred.");
//                logHelper.logError(logTransactionId, e);
//            } finally {
//                //TODO: disable in prod, implement retry mechanism
//                consumer.commitSync();
//            }
//        }
//    }
//
//
//
//    public Properties configure() {
//        Properties consumerConfig = new Properties();
//        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, groupID);
//        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
//        consumerConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
//        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
//        consumerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
//        consumerConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
//        consumerConfig.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1");
//
//        consumerConfig.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
//        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/var/run/secrets/java.io/truststores/user-truststore.jks");
//        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "changeit");
//        consumerConfig.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/var/run/secrets/java.io/keystores/user-keystore.jks");
//        consumerConfig.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "changeit");
//        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
//        return consumerConfig;
//    }
//}
