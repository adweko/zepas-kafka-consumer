/*
 * Copyright (C) 2019 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.mdm.config;

import lombok.Data;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.util.Base64;

@ApplicationScoped
@Data
public class WsConfiguration {


    @ConfigProperty(name = "ch.helvetia.zepas.endpoint")
    String wsUrlMdmFullPerson;

     String loggedInUser;
     byte[] samltokenSignerKeystore;
     String samltokenSignerPassword;


    public void setup() throws IOException {
        loggedInUser = "CHTI-MDM";
        samltokenSignerKeystore = Base64.getMimeDecoder().decode(System.getenv("ZEPAS_JKS"));
        samltokenSignerPassword = System.getenv("ZEPAS_JKS_PW");
    }
}
