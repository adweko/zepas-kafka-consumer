package com.helvetia.app.mdm.config;



import com.helvetia.app.mdm.streams.TopologyProducer;
import generated.MdmFullPersonV50_Service;
import generated.Status;
import generated.WsControl;
import lombok.Data;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import java.util.List;

@Data
@ApplicationScoped
public class ZepasConfig {
    final String ZEPAS = "CH_ZEPAS";
    final QName PORT_NAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/",
            "MdmFullPerson_v5_0Port");

    Holder<Long> personenLaufNummer = new Holder<>();
    Holder<List<Status>> status = new Holder<>();
    WsControl wsControl = new WsControl();
    MdmFullPersonV50_Service service = new MdmFullPersonV50_Service(TopologyProducer.class.getResource("/wsdl/MdmFullPerson_v5_0.wsdl"));
}
