/*
 * Copyright (C) 2018 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.mdm.config;

import com.helvetia.security.saml.SAMLAssertion;
import com.helvetia.security.saml.SAMLFactory;
import org.w3c.dom.Element;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Helper class to create SAML assertion
 *
 * @author CHE0MCR
 */
public class SAMLAssertionCreator {

	public Element getSAMLAssertion(WsConfiguration wsConfiguration, boolean printAssertion)
			throws Exception {
		InputStream in = new ByteArrayInputStream(wsConfiguration.getSamltokenSignerKeystore());
		KeyStore store = KeyStore.getInstance("JKS");
		store.load(in, null);
		SAMLFactory factory = SAMLFactory.getInstance();
		factory.initialize(store);
		SAMLAssertion samlAssertion = new SAMLAssertion();
		samlAssertion.setIdPrefix("aid");
		samlAssertion.setIncludeSignerCert(true);
		Calendar cal = Calendar.getInstance();
		Date dateNow = Date.from(LocalDateTime.now().minusSeconds(20).atZone(ZoneId.systemDefault()).toInstant());
		cal.setTime(dateNow);
		samlAssertion.setIssueInstant(cal);
		samlAssertion.setOneTimeUse(true);
		// assertion.setIssuer("<name des Verwenders (Applikationsname)>");
		samlAssertion.setIssuer("ZEPAS@web");
		// Subject: Tatsaechlich angemeldeter Benutzer, über den Principal des JAAS Subjects erhältlich
		samlAssertion.setSubject(wsConfiguration.getLoggedInUser());
		// Gueltigkeit 2 Minuten 120
		samlAssertion.setTtl(120000);
		// final String vaultString = getVaultString(properties.getProperty(Property.SAML_PASSWORD.toString()));
		// final String password = SecurityVaultUtil.getValueAsString(vaultString);
		final String password = wsConfiguration.getSamltokenSignerPassword();
		Element assertion = factory.generateDom(samlAssertion, password);
		if (printAssertion) {
			System.out.println(getAssertion(assertion));
			System.out.println("\nAssertion successfull created.");
		}
		return assertion;
	}

	private String getAssertion(Element assertion) throws Exception {
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		StringWriter buffer = new StringWriter();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(new DOMSource(assertion), new StreamResult(buffer));
		return buffer.toString();
	}
}
