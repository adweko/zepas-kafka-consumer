package com.helvetia.app.mdm;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;

@Liveness
@Readiness
@ApplicationScoped
public class CustomHealthCheck implements HealthCheck {
    @Override
    public HealthCheckResponse call() {
        try {
            return HealthCheckResponse.builder()
                    .name("ZEPASConsumerHealth")
                    .up()
                    .withData("status", "ok")
                    .build();
        } catch (Exception e) {
            return HealthCheckResponse.builder()
                    .name("ZEPASConsumerHealth")
                    .down()
                    .build();
        }
    }
}
