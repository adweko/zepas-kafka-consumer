package com.helvetia.app.mdm.mapping;

import com.helvetia.app.mdm.config.HelperWsClient;
import generated.MdmAdresse;
import generated.MdmCodeZurPerson;
import generated.MdmFinmaRegister;
import generated.MdmFullPerson;
import generated.MdmKommelementVerwendung;
import generated.MdmKommunikationselement;
import generated.MdmZusatzinformation;

import org.apache.commons.lang3.StringUtils;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasChannelID.hasChannelIDEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDSchema.hasAddressLocationID.hasAddressLocationIDSchema.AddressLocationID.AddressLocationIDSchema.hasAddressLocation.hasAddressLocationSchema.AddressLocation.AddressLocationEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasUnderwritingCodeID.hasUnderwritingCodeIDSchema.UnderwritingCodeID.UnderwritingCodeIDSchema.hasUnderwritingCode.hasUnderwritingCodeEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isLegalPerson.isLegalPersonSchema.LegalPerson.LegalPersonEntry;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class PersonMapper {

    protected void mapMdmChannels(List<hasChannelIDEntry> channels, MdmFullPerson mdmFullPerson) throws Exception {
        for (int i = 0; i < channels.size(); i++) {
            hasChannelIDEntry channel = channels.get(i);
            mapMdmChannel(channel, mdmFullPerson);
        }
    }

    private void mapMdmChannel(hasChannelIDEntry channel, MdmFullPerson mdmFullPerson) throws Exception {
        MdmKommunikationselement mdmKommunikationselement = new MdmKommunikationselement();
        if (channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0).getNoAds() != null) {
            MdmKommelementVerwendung mdmKommelementVerwendung = new MdmKommelementVerwendung();
            // TODO refactoring necessary if sample json file with mdmKommelementVerwendung exists
            //            mapMdmCommon(context, "ChannelID[0].????", mdmKommelementVerwendung, false);
            mdmKommelementVerwendung.setEingabeSb(getEingabeSb(String.valueOf(channel.getTECHUser())));
            //            mdmKommelementVerwendung.setKommelementLaufnummer(mdmKommunikationselement.getLaufnummer());
            //            mdmKommelementVerwendung
            //                    .setVerwendungCode(context.read("$.ChannelID[0].isChannel[0].Channel[0].noAds")); //001
            //            mdmKommelementVerwendung.setDelete(mdmKommunikationselement.isDelete());
            //            mdmKommelementVerwendung.setEingabeAm(mdmKommunikationselement.getEingabeAm());
            //            mdmKommelementVerwendung.setGueltigAb(mdmKommunikationselement.getGueltigAb());
            //            mdmKommelementVerwendung.setGueltigBis(mdmKommunikationselement.getGueltigBis());
            mdmKommelementVerwendung.setLaufnummer(0); // laufnummer released by ZEPAS!
            mdmKommelementVerwendung.setVerwendungCode("001"); // keine Werbung for KommElement
            mdmKommunikationselement.getMdmVerwendungen().add(mdmKommelementVerwendung);
        }

        switch (String.valueOf(channel.getChannelID().get(0).getChannelType())) {
        case "1":
            mapMdmCommonKommunikationselement(channel, mdmKommunikationselement);
            mdmKommunikationselement
                    .setElementtypCode(
                            channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0).getChannelNote()
                                    .toString());
            mdmKommunikationselement.setKommkanalCode("TEL");
            mdmKommunikationselement.setPrioritaet(0);
            mdmKommunikationselement
                    .setValue(channel.getChannelID().get(0).getHasTelephone().get(0).getTelephone().get(0)
                            .getTelephoneNumber().toString());
            mdmFullPerson.getMdmKommunikationselemente().add(mdmKommunikationselement);
            break;
        case "2":
            mapMdmCommonKommunikationselement(channel, mdmKommunikationselement);
            mdmKommunikationselement
                    .setElementtypCode(
                            channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0).getChannelNote()
                                    .toString());
            //mdmKommunikationselement.setKommentar("test");
            mdmKommunikationselement.setKommkanalCode("MOB");
            mdmKommunikationselement.setPrioritaet(0);
            mdmKommunikationselement.setValue(
                    channel.getChannelID().get(0).getHasMobile().get(0).getMobile().get(0).getMobileNumber()
                            .toString());
            mdmFullPerson.getMdmKommunikationselemente().add(mdmKommunikationselement);
            break;
        case "3":
            mapMdmCommonKommunikationselement(channel, mdmKommunikationselement);
            mdmKommunikationselement
                    .setElementtypCode(
                            channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0).getChannelNote()
                                    .toString());
            //mdmKommunikationselement.setKommentar("test");
            mdmKommunikationselement.setKommkanalCode("FAX");
            mdmKommunikationselement.setPrioritaet(0);
            mdmKommunikationselement.setValue(
                    channel.getChannelID().get(0).getHasFax().get(0).getFax().get(0).getFaxNumber().toString());
            mdmFullPerson.getMdmKommunikationselemente().add(mdmKommunikationselement);
            break;
        case "4":
            mapMdmCommonKommunikationselement(channel, mdmKommunikationselement);
            mdmKommunikationselement
                    .setElementtypCode(
                            channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0).getChannelNote()
                                    .toString());
            //mdmKommunikationselement.setKommentar("test");
            mdmKommunikationselement.setKommkanalCode("MAIL");
            mdmKommunikationselement.setPrioritaet(0);
            mdmKommunikationselement.setValue(
                    channel.getChannelID().get(0).getHasEmail().get(0).getEmail().get(0).getEmailAddress().toString());
            mdmFullPerson.getMdmKommunikationselemente().add(mdmKommunikationselement);
            break;
        case "5":
            mapMdmCommonKommunikationselement(channel, mdmKommunikationselement);
            mdmKommunikationselement
                    .setElementtypCode(
                            channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0).getChannelNote()
                                    .toString());
            //mdmKommunikationselement.setKommentar("test");
            mdmKommunikationselement.setKommkanalCode("SKYPE");
            mdmKommunikationselement.setPrioritaet(0);
            mdmKommunikationselement.setValue(
                    channel.getChannelID().get(0).getHasSkype().get(0).getSkype().get(0).getSkypeAddress().toString());
            mdmFullPerson.getMdmKommunikationselemente().add(mdmKommunikationselement);
            break;
        case "6":
            mdmFullPerson.getMdmAdressen().add(mapMdmWohnGeschaeftAdresse(channel));
            break;
        case "7":
            mdmFullPerson.getMdmAdressen().add(mapMdmZweitsitzAdresse(channel));
            break;
        case "8":
            mdmFullPerson.getMdmAdressen().add(mapMdmPostfachAdresse(channel));
            break;

        }
    }

    private void mapMdmCommonKommunikationselement(hasChannelIDEntry channel,
            MdmKommunikationselement mdmKommunikationselement) throws Exception {
        LocalDate gueltigAb = LocalDate
                .of(channel.getChannelID().get(0).getIsChannel().get(0).getBITEMPValidFrom().getYear(),
                        channel.getChannelID().get(0).getIsChannel().get(0).getBITEMPValidFrom().getMonth(),
                        channel.getChannelID().get(0).getIsChannel().get(0).getBITEMPValidFrom().getDay());
        if (gueltigAb.isBefore(LocalDate.now())) {
            mdmKommunikationselement.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        } else {
            mdmKommunikationselement.setGueltigAb(HelperWsClient.formatLocalDate(gueltigAb));
        }
        LocalDate gueltigBis = LocalDate
                .of(channel.getChannelID().get(0).getIsChannel().get(0).getBITEMPValidTo().getYear(),
                        channel.getChannelID().get(0).getIsChannel().get(0).getBITEMPValidTo().getMonth(),
                        channel.getChannelID().get(0).getIsChannel().get(0).getBITEMPValidTo().getDay());
        if (gueltigBis.isBefore(LocalDate.of(9999, 12, 31)) &&
                gueltigBis.isBefore(LocalDate.of(3000, 01, 01))) {
            mdmKommunikationselement.setGueltigBis(HelperWsClient.formatLocalDate(gueltigBis));
        } else {
            mdmKommunikationselement.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        }
        mdmKommunikationselement.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmKommunikationselement.setEingabeSb(getEingabeSb(String.valueOf(channel.getTECHUser())));
        mdmKommunikationselement.setLaufnummer(Long.parseLong(
                channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0)
                        .getIDNumber().toString()));
    }

    private MdmAdresse mapMdmWohnGeschaeftAdresse(hasChannelIDEntry channel) throws Exception {
        MdmAdresse mdmAdresse = new MdmAdresse();
        mapMdmCommonAdresse(channel, mdmAdresse);
        mdmAdresse.setAdressartCode("001");
        mdmAdresse.setStrassenname(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getStreet(), null));
        mdmAdresse.setHausnummer(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getHouseNumber(), null));

        return mdmAdresse;
    }

    private MdmAdresse mapMdmZweitsitzAdresse(hasChannelIDEntry channel) throws Exception {
        MdmAdresse mdmAdresse = new MdmAdresse();
        mapMdmCommonAdresse(channel, mdmAdresse);
        mdmAdresse.setAdressartCode("002");
        mdmAdresse.setStrassenname(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getStreet(), null));
        mdmAdresse.setHausnummer(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getHouseNumber(), null));
        return mdmAdresse;
    }

    private MdmAdresse mapMdmPostfachAdresse(hasChannelIDEntry channel) throws Exception {
        MdmAdresse mdmAdresse = new MdmAdresse();
        mapMdmCommonAdresse(channel, mdmAdresse);
        mdmAdresse.setAdressartCode("003");
        mdmAdresse.setPostfachnummer(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getPostbox(), null));
        mdmAdresse.setStrassenname(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getStreet(), null));
        mdmAdresse.setHausnummer(Objects.toString(
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID()
                        .get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0)
                        .getHouseNumber(), null));
        return mdmAdresse;
    }

    private void mapMdmCommonAdresse(hasChannelIDEntry channel, MdmAdresse mdmAdresse) throws Exception {
        mdmAdresse.setLaufnummer(Long.parseLong(
                channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0)
                        .getIDNumber().toString()));
        LocalDate gueltigAb = LocalDate
                .of(channel.getChannelID().get(0).getHasLocationID().get(0)
                                .getBITEMPValidFrom().getYear(),
                        channel.getChannelID().get(0).getHasLocationID().get(0)
                                .getBITEMPValidFrom().getMonth(),
                        channel.getChannelID().get(0).getHasLocationID().get(0)
                                .getBITEMPValidFrom().getDay());
        if (gueltigAb.isBefore(LocalDate.now())) {
            mdmAdresse.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        } else {
            mdmAdresse.setGueltigAb(HelperWsClient.formatLocalDate(gueltigAb));
        }
        LocalDate gueltigBis = LocalDate
                .of(channel.getChannelID().get(0).getHasLocationID().get(0)
                                .getBITEMPValidTo().getYear(),
                        channel.getChannelID().get(0).getHasLocationID().get(0)
                                .getBITEMPValidTo().getMonth(),
                        channel.getChannelID().get(0).getHasLocationID().get(0)
                                .getBITEMPValidTo().getDay());
        if (gueltigBis.isBefore(LocalDate.of(9999, 12, 31)) &&
                gueltigBis.isBefore(LocalDate.of(3000, 01, 01))) {
            mdmAdresse.setGueltigBis(HelperWsClient.formatLocalDate(gueltigBis));
        } else {
            mdmAdresse.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        }
        mdmAdresse.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmAdresse.setEingabeSb(getEingabeSb(String.valueOf(channel.getTECHUser())));
        AddressLocationEntry addressLocationEntry = channel.getChannelID().get(0).getHasLocationID().get(0)
                .getLocationID().get(0).getHasAddressLocationID().get(0).getAddressLocationID().get(0)
                .getHasAddressLocation().get(0).getAddressLocation().get(0);
        mdmAdresse.setPostleitzahl(Objects.toString(addressLocationEntry.getZIPCode(), null));
        mdmAdresse.setOrtsname(Objects.toString(addressLocationEntry.getCity(), null));
        if (channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0)
                .getRegion().get(0) != null) {
            mdmAdresse.setRegion(Objects.toString(
                    channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0)
                            .getRegion().get(0).getRegionName(), null));
        }
        mdmAdresse.setLandkennzeichenCode(String.valueOf(addressLocationEntry.getCountry()));
        if (channel.getChannelID().get(0).getHasSite() != null) {
            mdmAdresse.setWohnungsbezeichnung(
                    Objects.toString(channel.getChannelID().get(0).getHasSite().get(0).getSite().get(0).getFlat(),
                            null));
            mdmAdresse.setLokalitaetsbezeichnung(Objects.toString(
                    channel.getChannelID().get(0).getHasSite().get(0).getSite().get(0).getLocationDescription(), null));
        }

    }

    protected void mapMdmFinmaregister(VersionsEntry stateMessage, MdmFullPerson mdmFullPerson) throws Exception {
        // TODO Workaround as long as Finmaregister node not exists in JSON file
        if (mdmFullPerson.getMdmAdressen().isEmpty() || mdmFullPerson.getMdmAdressen().size() == 0) {
            return;
        }
        // Finmaregister for CH and FL countries only
        String country = mdmFullPerson.getMdmAdressen().get(0).getLandkennzeichenCode();
        if (country.equalsIgnoreCase("CH") || country.equalsIgnoreCase("FL")) {
            MdmFinmaRegister mdmFinmaRegister = new MdmFinmaRegister();
            mdmFinmaRegister.setLaufnummer(0);  // laufnummer released by ZEPAS!
            mdmFinmaRegister.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
            mdmFinmaRegister.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
            mdmFinmaRegister.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
            mdmFinmaRegister.setLandCode(country);
            // TODO missing RegisterNummer in JSON file!
            LegalPersonEntry legalPerson = stateMessage.getIsLegalPerson().get(0).getLegalPerson().get(0);
            mdmFinmaRegister.setRegisterNummer(legalPerson.getFINMANumber().toString());
            mdmFinmaRegister.setPersonenLaufnummer(mdmFullPerson.getMdmCompositePerson().getMdmPersonenLaufnummer());
            mdmFullPerson.getMdmFinmaRegisters().add(mdmFinmaRegister);
        }
    }

    protected void mapMdmZusatzinformation(MdmFullPerson mdmFullPerson) throws Exception {
        // TODO Workaround as long as Zusatzinformation node not exists in JSON file
        MdmZusatzinformation mdmZusatzinformation = new MdmZusatzinformation();
        mdmZusatzinformation.setLaufnummer(0);  // laufnummer released by ZEPAS!
        mdmZusatzinformation.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmZusatzinformation.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmZusatzinformation.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmZusatzinformation.setInfoartCode("102"); // keine Werbung erwünscht
        mdmZusatzinformation.setZuordnung(false);
        mdmZusatzinformation.setPersonenLaufnummer(mdmFullPerson.getMdmCompositePerson().getMdmPersonenLaufnummer());
        mdmFullPerson.getMdmZusatzinformationen().add(mdmZusatzinformation);
    }

    protected void mapMdmCodesZurPerson(VersionsEntry stateMessage, MdmFullPerson mdmFullPerson) throws Exception {
        List<hasUnderwritingCodeEntry> uwCodes = stateMessage.getHasUnderwritingCodeID().get(0).getUnderwritingCodeID().get(0).getHasUnderwritingCode();
        for (int i = 0; i < uwCodes.size(); i++) {
            hasUnderwritingCodeEntry uwCode = uwCodes.get(i);
            mapMdmCodeZurPerson(uwCode, mdmFullPerson);
        }
    }

    private void mapMdmCodeZurPerson(hasUnderwritingCodeEntry uwCode, MdmFullPerson mdmFullPerson) throws Exception {
        MdmCodeZurPerson mdmCodeZurPerson = new MdmCodeZurPerson();

        LocalDate gueltigAb = LocalDate.of(uwCode.getBITEMPValidFrom().getYear(),
                uwCode.getBITEMPValidFrom().getMonth(),
                uwCode.getBITEMPValidFrom().getDay());
        if (gueltigAb.isBefore(LocalDate.now())) {
            mdmCodeZurPerson.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        } else {
            mdmCodeZurPerson.setGueltigAb(HelperWsClient.formatLocalDate(gueltigAb));
        }
        LocalDate gueltigBis = LocalDate.of(uwCode.getBITEMPValidTo().getYear(),
                uwCode.getBITEMPValidTo().getMonth(),
                uwCode.getBITEMPValidTo().getDay());
        if (gueltigBis.isBefore(LocalDate.of(9999, 12, 31)) &&
                gueltigBis.isBefore(LocalDate.of(3000, 01, 01))) {
            mdmCodeZurPerson.setGueltigBis(HelperWsClient.formatLocalDate(gueltigBis));
        } else {
            mdmCodeZurPerson.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        }
        mdmCodeZurPerson.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        // TODO Workaround as long as TECHUser node not exists on person or sub elements of person communication, address, ...
        mdmCodeZurPerson.setEingabeSb(getEingabeSb(String.valueOf(uwCode.getTECHUser())));

        //        mdmCodeZurPerson.setStopCode(String.valueOf(uwCode.getUnderwritingCode().get(0).getStopCode()));
        //        mdmCodeZurPerson.setEingabeSb(uwCode.getUnderwritingCode().get(0).getTECHUser().toString());
        mdmCodeZurPerson.setPersonenLaufnummer(mdmFullPerson.getMdmCompositePerson().getMdmPersonenLaufnummer());
        mdmFullPerson.getMdmCodesZurPerson().add(mdmCodeZurPerson);
    }

    protected String getEingabeSb(String eingabeSb) {
        // to prevent NPE!!
        return StringUtils.isEmpty(eingabeSb) || eingabeSb.equals("null") ? "CHTI-MDM" : eingabeSb;
    }
}
