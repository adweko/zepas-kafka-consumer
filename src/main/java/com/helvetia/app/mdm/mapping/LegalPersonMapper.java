package com.helvetia.app.mdm.mapping;

import com.helvetia.app.mdm.config.HelperWsClient;
import generated.MdmCompositePerson;
import generated.MdmFullPerson;
import generated.MdmUnternehmung;

import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isLegalPerson.isLegalPersonSchema.LegalPerson.LegalPersonEntry;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.Objects;

@ApplicationScoped
public class LegalPersonMapper extends PersonMapper {

    MdmFullPerson mdmFullPerson = new MdmFullPerson();
    MdmUnternehmung mdmUnternehmung = new MdmUnternehmung();
    MdmCompositePerson mdmCompositePerson = new MdmCompositePerson();

    public MdmFullPerson mapMdmFullPerson(VersionsEntry stateMessage) throws Exception {
        mdmFullPerson = new MdmFullPerson();
        mdmUnternehmung = new MdmUnternehmung();
        mdmCompositePerson = new MdmCompositePerson();
        mdmUnternehmung = mapMdmLegalPerson(stateMessage);
        mdmCompositePerson.setMdmUnternehmung(mdmUnternehmung);
        mdmCompositePerson.setMdmPersonenLaufnummer(mdmUnternehmung.getLaufnummer());
        mdmFullPerson.setMdmCompositePerson(mdmCompositePerson);
        mapMdmChannels(stateMessage.getHasChannelID(), mdmFullPerson);
        LegalPersonEntry legalPerson = stateMessage.getIsLegalPerson().get(0).getLegalPerson().get(0);
        if (legalPerson.getFINMANumber() != null && !String.valueOf(legalPerson.getFINMANumber()).equals("n/a")) {
            mapMdmFinmaregister(stateMessage, mdmFullPerson);
        }
        if (legalPerson.getNoAds() != null) {//TODO NoAds missing in Schema
            mapMdmZusatzinformation(mdmFullPerson);
        }
//        if (stateMessage.getHasUnderwritingCode() != null && stateMessage.getHasUnderwritingCode().get(0).getUnderwritingCode().get(0).getStopCode() != null)
//            mapMdmCodesZurPerson(stateMessage, mdmFullPerson);
        return mdmFullPerson;
    }

    private MdmUnternehmung mapMdmLegalPerson(VersionsEntry stateMessage) throws Exception {
        MdmUnternehmung mdmUnternehmung = new MdmUnternehmung();
        LocalDate gueltigAb = LocalDate.of(stateMessage.getIsLegalPerson().get(0).getBITEMPValidFrom().getYear(),
                stateMessage.getIsLegalPerson().get(0).getBITEMPValidFrom().getMonth(),
                stateMessage.getIsLegalPerson().get(0).getBITEMPValidFrom().getDay());
        if (gueltigAb.isBefore(LocalDate.now())) {
            mdmUnternehmung.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        } else {
            mdmUnternehmung.setGueltigAb(HelperWsClient.formatLocalDate(gueltigAb));
        }
        LocalDate gueltigBis = LocalDate.of(stateMessage.getIsLegalPerson().get(0).getBITEMPValidTo().getYear(),
                stateMessage.getIsLegalPerson().get(0).getBITEMPValidTo().getMonth(),
                stateMessage.getIsLegalPerson().get(0).getBITEMPValidTo().getDay());
        if (gueltigBis.isBefore(LocalDate.of(9999, 12, 31)) &&
                gueltigBis.isBefore(LocalDate.of(3000, 01, 01))) {
            mdmUnternehmung.setGueltigBis(HelperWsClient.formatLocalDate(gueltigBis));
        } else {
            mdmUnternehmung.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        }
        mdmUnternehmung.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        // che0bmc: Technischer User wird immer im IsBusinessPartner geliefert
        mdmUnternehmung.setEingabeSb(getEingabeSb(String.valueOf(stateMessage.getIsBusinessPartner().get(0).getTECHUser())));
        mdmUnternehmung.setLaufnummer(
                Long.parseLong(stateMessage.getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString()));
        mdmUnternehmung.setKennzeichenCode("U");
        LegalPersonEntry legalPerson = stateMessage.getIsLegalPerson().get(0).getLegalPerson().get(0);
        mdmUnternehmung.setSprachCode(Objects.toString(legalPerson.getLanguage(), null));
        mdmUnternehmung.setAnredeCode("");
        mdmUnternehmung.setName(Objects.toString(legalPerson.getCompanyName(), null));
        mdmUnternehmung.setNamenZusatz1(Objects.toString(legalPerson.getNameAddition(), null));
        mdmUnternehmung.setNamenZusatz2(Objects.toString(legalPerson.getNameAddition2(), null));
        mdmUnternehmung.setNogaCode(Objects.toString(legalPerson.getNOGACode(), null));
        mdmUnternehmung.setRechtsformCode(Objects.toString(legalPerson.getLegalType(), null));

        // End end of missing ...
        String uid = "";
        if (legalPerson.getUID() != null) {
            uid = String.valueOf(legalPerson.getUID()).replace(".", "").replace("-", "").toUpperCase();
        }
        mdmUnternehmung.setUnternehmensId(uid);
        if (legalPerson.getClosingDate() != null) {
            mdmUnternehmung.setAufloesungsdatum(HelperWsClient
                    .formatLocalDate(LocalDate.of(legalPerson.getClosingDate().getYear(),
                            legalPerson.getClosingDate().getMonth(),
                            legalPerson.getClosingDate().getDay())));
        }
//        mdmUnternehmung.setRabattklasseCode(legalPerson.getDiscountCategory());
        mdmUnternehmung.setMehrwertssteuernummer(0L);
        mdmUnternehmung.setGegenparteiId("");
        mdmUnternehmung.setStiftungstext1("");
        mdmUnternehmung.setStiftungstext2("");
        return mdmUnternehmung;
    }
}
