package com.helvetia.app.mdm.mapping;

import com.helvetia.app.mdm.config.HelperWsClient;
import generated.MdmCompositePerson;
import generated.MdmFullPerson;
import generated.MdmPrivatperson;

import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isNaturalPerson.isNaturalPersonSchema.NaturalPerson.NaturalPersonEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class NaturalPersonMapper extends PersonMapper {

    MdmFullPerson mdmFullPerson = new MdmFullPerson();
    MdmPrivatperson mdmPrivatperson = new MdmPrivatperson();
    MdmCompositePerson mdmCompositePerson = new MdmCompositePerson();

    public MdmFullPerson mapMdmFullPerson(readFullPartnerAllVersionsFromDateResponse mdmState) throws Exception {
        List<VersionsEntry> versions = mdmState.getBusinessPartnerID().get(0).getVersions();
        VersionsEntry stateMessage = versions.get(0);
        mdmFullPerson = new MdmFullPerson();
        mdmPrivatperson = new MdmPrivatperson();
        mdmCompositePerson = new MdmCompositePerson();
        mdmPrivatperson = mapMdmNaturalPerson(stateMessage);
        mdmCompositePerson.setMdmPrivatperson(mdmPrivatperson);
        mdmCompositePerson.setMdmPersonenLaufnummer(mdmPrivatperson.getLaufnummer());
        mdmFullPerson.setMdmCompositePerson(mdmCompositePerson);
        for (int i = 0; i < versions.size() && i <= 1; i++) {
            stateMessage = versions.get(i);
            mapMdmChannels(stateMessage.getHasChannelID(), mdmFullPerson);
        }
        if (stateMessage.getIsNaturalPerson().get(0).getNaturalPerson().get(0).getNoAds() != null) {
            mapMdmZusatzinformation(mdmFullPerson);
        }
//        if (stateMessage.getHasUnderwritingCode().get(0).getUnderwritingCode().get(0).getStopCode() != null)
//            mapMdmCodesZurPerson(stateMessage, mdmFullPerson);
        return mdmFullPerson;
    }

    private MdmPrivatperson mapMdmNaturalPerson(VersionsEntry stateMessage) throws Exception {
        MdmPrivatperson mdmPrivatperson = new MdmPrivatperson();
        LocalDate gueltigAb = LocalDate.of(stateMessage.getIsNaturalPerson().get(0).getBITEMPValidFrom().getYear(),
                stateMessage.getIsNaturalPerson().get(0).getBITEMPValidFrom().getMonth(),
                stateMessage.getIsNaturalPerson().get(0).getBITEMPValidFrom().getDay());
        if (gueltigAb.isBefore(LocalDate.now())) {
            mdmPrivatperson.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        } else {
            mdmPrivatperson.setGueltigAb(HelperWsClient.formatLocalDate(gueltigAb));
        }
        LocalDate gueltigBis = LocalDate.of(stateMessage.getIsNaturalPerson().get(0).getBITEMPValidTo().getYear(),
                stateMessage.getIsNaturalPerson().get(0).getBITEMPValidTo().getMonth(),
                stateMessage.getIsNaturalPerson().get(0).getBITEMPValidTo().getDay());
        if (gueltigBis.isBefore(LocalDate.of(9999, 12, 31)) &&
                gueltigBis.isBefore(LocalDate.of(3000, 01, 01))) {
            mdmPrivatperson.setGueltigBis(HelperWsClient.formatLocalDate(gueltigBis));
        } else {
            mdmPrivatperson.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        }
        mdmPrivatperson.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        // che0bmc: Technischer User wird immer im IsBusinessPartner geliefert
        mdmPrivatperson.setEingabeSb(getEingabeSb(String.valueOf(stateMessage.getIsBusinessPartner().get(0).getTECHUser())));
        mdmPrivatperson.setLaufnummer(
                Long.parseLong(stateMessage.getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString()));
        mdmPrivatperson.setKennzeichenCode("P");
        NaturalPersonEntry naturalPerson = stateMessage.getIsNaturalPerson().get(0).getNaturalPerson().get(0);
        mdmPrivatperson.setSprachCode(Objects.toString(naturalPerson.getLanguage(), null));
        mdmPrivatperson.setName(Objects.toString(naturalPerson.getSurname(), null));
        mdmPrivatperson.setVorname(Objects.toString(naturalPerson.getName(), null));
        mdmPrivatperson.setNamenZusatz1(Objects.toString(naturalPerson.getSecondName(), null));
        mdmPrivatperson.setNamenZusatz2(Objects.toString(naturalPerson.getThirdName(), null));
        mdmPrivatperson.setAkademischerTitelCode(Objects.toString(naturalPerson.getAcademicDegree(), null));
        mdmPrivatperson.setSonstigeTitel(Objects.toString(naturalPerson.getOtherTitle(), null));

        // haben wir ein Geschlecht, dann dieses sezen
        if (naturalPerson.getSex() != null) {
            mdmPrivatperson.setGeschlechtCode(Objects.toString(naturalPerson.getSex(), null));
        }
        // haben wir einen AnredeCode?
        if (naturalPerson.getSalutation() != null) {
            mdmPrivatperson.setAnredeCode(Objects.toString(naturalPerson.getSalutation(), null));
        } else if ((mdmPrivatperson.getGeschlechtCode() != null ) && mdmPrivatperson.getGeschlechtCode().equals("1")) {
            // keinen Anredecode, dann Anrede aus Geschlecht Mann
            mdmPrivatperson.setAnredeCode("01");
        } else if ((mdmPrivatperson.getGeschlechtCode() != null ) && mdmPrivatperson.getGeschlechtCode().equals("2")) {
            // keinen Anredecode, dann Anrede aus Geschlecht Frau
            mdmPrivatperson.setAnredeCode("02");
            if (mdmPrivatperson.getAkademischerTitelCode() != null && mdmPrivatperson.getAkademischerTitelCode()
                    .equals("-01")) {
                // Bei Nonnen: AnredeCode 7 setzen und akademischen Titel entfernen
                mdmPrivatperson.setAnredeCode("07");
                mdmPrivatperson.setAkademischerTitelCode(null);
            }
        }

        mdmPrivatperson.setGeburtsname(Objects.toString(naturalPerson.getNameAtBirth(), null));
        if (naturalPerson.getDateOfBirth() != null) {
            mdmPrivatperson.setGeburtsdatum(HelperWsClient.formatLocalDate(
                    LocalDate.of(naturalPerson.getDateOfBirth().getYear(),
                            naturalPerson.getDateOfBirth().getMonth(),
                            naturalPerson.getDateOfBirth().getDay())));
        }
        if (naturalPerson.getDateOfDeath() != null) {
            mdmPrivatperson.setTodesdatum(HelperWsClient.formatLocalDate(
                    LocalDate.of(naturalPerson.getDateOfDeath().getYear(),
                            naturalPerson.getDateOfDeath().getMonth(),
                            naturalPerson.getDateOfDeath().getDay())));
        }
        mdmPrivatperson.setAnstellungsVerhaeltnisCode(Objects.toString(naturalPerson.getEmploymentStatus(), null));
        mdmPrivatperson.setBuergerort(Objects.toString(naturalPerson.getHomeTown(), null));
        mdmPrivatperson.setAufenthaltsbewilligungCode(Objects.toString(naturalPerson.getResidencePermit(), null));
        mdmPrivatperson.setBeruflicheTaetigkeitCode(Objects.toString(naturalPerson.getFunction(), null));
        if (naturalPerson.getSocialSecurityNumber() != null) {
            Long sozialVerNr = Long.parseLong(naturalPerson.getSocialSecurityNumber().toString()
                    .replace(".", ""));
            mdmPrivatperson.setSozialversicherungsnummer(sozialVerNr);
        }
        mdmPrivatperson.setNationalitaetCode(Objects.toString(naturalPerson.getNationality(), null));
        mdmPrivatperson.setZivilstandsCode(Objects.toString(naturalPerson.getMaritalStatus(), null));
        //mdmPrivatperson.setRabattklasseCode(context.read("$.isNaturalPerson[0].NaturalPerson[0].discountCategory"));
        //        MdmMitarbeiter mdmMitarbeiter = new MdmMitarbeiter();
        //        mdmMitarbeiter.isMitarbeiterkennzeichen(context.read("$.isNaturalPerson[0].NaturalPerson[0].employee"));
        //        mdmPrivatperson.setMdmMitarbeiter();
        return mdmPrivatperson;
    }
}
