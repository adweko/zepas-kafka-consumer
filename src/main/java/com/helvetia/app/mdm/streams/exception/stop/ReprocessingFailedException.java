package com.helvetia.app.mdm.streams.exception.stop;

public class ReprocessingFailedException extends StopJobException {
    public ReprocessingFailedException() {
        super();
    }

    public ReprocessingFailedException(String message) {
        super(message);
    }

    public ReprocessingFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReprocessingFailedException(Throwable cause) {
        super(cause);
    }
}
