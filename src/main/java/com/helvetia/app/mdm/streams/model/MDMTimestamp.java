package com.helvetia.app.mdm.streams.model;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * timestamps come as 20200618144950121
 * corresponding to   yyyyMMddHHmmssSSS
 */
public class MDMTimestamp {

    private final String rawTimestamp;

    private final Instant instant;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS").withZone(ZoneId.of("UTC"));

    public MDMTimestamp(String rawTimestamp) {
        this.rawTimestamp = rawTimestamp;
        this.instant = Instant.from(formatter.parse(rawTimestamp));
    }

    public MDMTimestamp(Instant instant) {
        this.instant = instant;
        this.rawTimestamp = formatter.format(instant);
    }

    public Instant getInstant() {
        return instant;
    }

    public String getRawTimestamp() {
        return rawTimestamp;
    }
}
