package com.helvetia.app.mdm.streams.processor;


import ch.mdm.zepas.dlq.DLQMessage;
import ch.mdm.zepas.dlq.DLQMessages;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.app.mdm.streams.TopologyProducer;
import com.helvetia.app.mdm.streams.model.ResponseOrDlqmessages;
import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class IsPartnerLockedTransformer implements Transformer<TechMDMPartnerID, readFullPartnerAllVersionsFromDateResponse, KeyValue<TechMDMPartnerID, ResponseOrDlqmessages>> {

    private ProcessorContext context;
    private TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages> dlqLocalStore;

    @Inject
    LogHelper logHelper;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
        dlqLocalStore = (TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages>) context.getStateStore(TopologyProducer.DLQ_STORE_NAME);
    }

    @Override
    public KeyValue<TechMDMPartnerID, ResponseOrDlqmessages> transform(TechMDMPartnerID techMDMPartnerID, readFullPartnerAllVersionsFromDateResponse response) {

        String logTransactionId = "";
        try {
            logTransactionId = LogHelper.getTechMdmPartnerID(response.getBusinessPartnerID().get(0))
                    + "|" + LogHelper.getPartnerId(response.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                    + "|" + LogHelper.getPartnerId(response.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);
        } catch (Exception e) {
            logHelper.logError("", "", "could not extract transaction id from message");
        }

        ValueAndTimestamp<DLQMessages> dlqMessagesValueAndTimestamp = dlqLocalStore.get(techMDMPartnerID);
        if (dlqMessagesValueAndTimestamp != null) {
            DLQMessages lockedMessages = dlqMessagesValueAndTimestamp.value();
            logHelper.logInfo("unknown user", logTransactionId, context, "message send to DLQ because partner " + techMDMPartnerID.getValue() + " is " +
                    "still locked.");
            lockedMessages.getMessages().add(new DLQMessage(context.topic(), context.partition(), context.offset(), context.timestamp(), response));
            return new KeyValue<>(techMDMPartnerID, new ResponseOrDlqmessages(lockedMessages));
        }
        return new KeyValue<>(techMDMPartnerID, new ResponseOrDlqmessages(response));
    }

    @Override
    public void close() {

    }
}
