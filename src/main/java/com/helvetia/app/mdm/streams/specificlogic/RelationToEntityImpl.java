package com.helvetia.app.mdm.streams.specificlogic;


import com.helvetia.app.mdm.streams.RelationToEntity;
import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RelationToEntityImpl implements RelationToEntity<readFullPartnerAllVersionsFromDateResponse, TechMDMPartnerID> {
    @Override
    public boolean canBeRelatedToEntity(readFullPartnerAllVersionsFromDateResponse message) {
        final CharSequence techmdmPartnerID = getTechmdmPartnerID(message);
        return techmdmPartnerID != null && !techmdmPartnerID.toString().isEmpty();
    }

    @Override
    public TechMDMPartnerID extractEntity(readFullPartnerAllVersionsFromDateResponse message) {
        final CharSequence techmdmPartnerIDChars = getTechmdmPartnerID(message);
        return new TechMDMPartnerID(techmdmPartnerIDChars.toString());
    }

    private CharSequence getTechmdmPartnerID(readFullPartnerAllVersionsFromDateResponse message) {
        return message.getBusinessPartnerID().get(0).getTECHMDMPartnerID();
    }
}
