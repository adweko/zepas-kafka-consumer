package com.helvetia.app.mdm.streams.exception.stop;

public class SendingFailedException extends StopJobException {
    public SendingFailedException() {
        super();
    }

    public SendingFailedException(String message) {
        super(message);
    }

    public SendingFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public SendingFailedException(Throwable cause) {
        super(cause);
    }
}
