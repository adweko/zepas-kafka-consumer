package com.helvetia.app.mdm.streams;

import com.helvetia.app.mdm.streams.model.ResultOrExceptionWithInput;

public interface MapperAndSender<I,O>  {
    ResultOrExceptionWithInput<I, O> mapAndSend(I input);

}
