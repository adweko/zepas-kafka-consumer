package com.helvetia.app.mdm.streams.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Objects;

/**
 * Republishing messages arriving in ch-mdm.unlock.partner, like this>
 * key: {"TECHMDMPartnerID":"E107B98D-5083-436D-9513-D626B9C475BC","TECHExternalPID":"189f027d-7028-4aa2-9f02-7d70284aa255","TECHSourceSystem":"MDM","BITEMPMDMTimestamp":"2020-08-20T08:57:22+0000"}
 * value: {"TECHMDMPartnerID":"E107B98D-5083-436D-9513-D626B9C475BC","RepublishedAt":"20200819152152623"}
 */
public class UnlockMessage {

    public static final Serde<UnlockMessage> serde = new Serde<>() {
        @Override
        public Serializer<UnlockMessage> serializer() {
            return (topic, data) -> Serdes.String().serializer().serialize(topic, data.toJsonString());
        }

        @Override
        public Deserializer<UnlockMessage> deserializer() {
            return (topic, data) -> UnlockMessage.fromJsonString(Serdes.String().deserializer().deserialize(topic, data));
        }
    };

    public static final String PARTNER_ID_FIELD_NAME = "TECHMDMPartnerID";
    public static final String REPUBLISHED_AT_FIELD_NAME = "RepublishedAt";

    private final TechMDMPartnerID partnerId;
    private final MDMTimestamp republishedAt;

    private final ObjectMapper mapper = new ObjectMapper();

    private UnlockMessage(String jsonRepresentation) {
        try {
            final JsonNode jsonNode = mapper.readTree(jsonRepresentation);
            this.partnerId = new TechMDMPartnerID(jsonNode.get(PARTNER_ID_FIELD_NAME).asText());
            this.republishedAt = new MDMTimestamp(jsonNode.get(REPUBLISHED_AT_FIELD_NAME).asText());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    public UnlockMessage(TechMDMPartnerID techMDMPartnerID, MDMTimestamp mdmTimestamp) {
        this.partnerId = techMDMPartnerID;
        this.republishedAt = mdmTimestamp;
    }

    public static UnlockMessage create(TechMDMPartnerID techMDMPartnerID, MDMTimestamp mdmTimestamp) {
        return new UnlockMessage(techMDMPartnerID, mdmTimestamp);
    }

    public String toJsonString() {
        try {
            final ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
            objectNode.put(PARTNER_ID_FIELD_NAME, partnerId.getValue());
            objectNode.put(REPUBLISHED_AT_FIELD_NAME, republishedAt.getRawTimestamp());
            return mapper.writeValueAsString(objectNode);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static UnlockMessage fromJsonString(String jsonRepresentation) {
        return new UnlockMessage(jsonRepresentation);
    }

    public TechMDMPartnerID getPartnerId() {
        return partnerId;
    }

    public MDMTimestamp getRepublishedAt() {
        return republishedAt;
    }

    @Override
    public String toString() {
        return "UnlockMessage{" +
                "partnerId=" + partnerId +
                ", republishedAt=" + republishedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnlockMessage that = (UnlockMessage) o;
        return Objects.equals(partnerId, that.partnerId) &&
                Objects.equals(republishedAt, that.republishedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(partnerId, republishedAt);
    }
}
