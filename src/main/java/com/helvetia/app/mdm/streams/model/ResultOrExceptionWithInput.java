package com.helvetia.app.mdm.streams.model;


public class ResultOrExceptionWithInput<V, R> {
    private final V input;
    private final R result;
    private final Exception exception;


    public ResultOrExceptionWithInput(R result) {
        this.result = result;
        this.input = null;
        this.exception = null;

    }

    public ResultOrExceptionWithInput(V input, Exception e) {
        this.input = input;
        this.exception = e;
        this.result = null;
    }

    public V getInput() {
        return input;
    }

    public R getResult() {
        return result;
    }

    public Exception getException() {
        return exception;
    }

    public boolean isResult() {
        return getResult() != null;
    }

    public boolean isException() {
        return !isResult();
    }

}
