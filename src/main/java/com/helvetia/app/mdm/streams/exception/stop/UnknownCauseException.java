package com.helvetia.app.mdm.streams.exception.stop;

public class UnknownCauseException extends StopJobException {
    public UnknownCauseException() {
        super();
    }

    public UnknownCauseException(String message) {
        super(message);
    }

    public UnknownCauseException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownCauseException(Throwable cause) {
        super(cause);
    }
}
