package com.helvetia.app.mdm.streams.specificlogic;

import com.helvetia.app.mdm.config.ZepasConfig;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.app.mdm.streams.RelevantMessageFilter;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasChannelID.hasChannelIDEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class RelevantMessageFilterImpl implements RelevantMessageFilter<readFullPartnerAllVersionsFromDateResponse> {

    @Inject
    ZepasConfig zepasConfig;

    @Inject
    LogHelper logHelper;

    @Override
    public boolean isMessageRelevant(readFullPartnerAllVersionsFromDateResponse message) {
        if (message.getBusinessPartnerID().get(0).getVersions().get(0).getIsNaturalPerson() != null &&
                message.getBusinessPartnerID().get(0).getVersions().get(0).getIsNaturalPerson().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getVersions().get(0).getIsBusinessPartner().get(0).getTECHSourceSystem().toString().equals(zepasConfig.getZEPAS())) {
            return true;
        }
        if (message.getBusinessPartnerID().get(0).getVersions().get(0).getIsLegalPerson() != null &&
                message.getBusinessPartnerID().get(0).getVersions().get(0).getIsLegalPerson().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getVersions().get(0).getIsBusinessPartner().get(0).getTECHSourceSystem().toString().equals(zepasConfig.getZEPAS())) {
            return true;
        }
        List<hasChannelIDEntry> channelList = message.getBusinessPartnerID().get(0).getVersions().get(0).getHasChannelID();
        if (channelList != null) {
            for (hasChannelIDEntry channel : channelList) {
                if (!String.valueOf(channel.getTECHSourceSystem()).equals(zepasConfig.getZEPAS()) && channel.getTECHSourceSystem() != null) {
                    return true;
                }
            }
        }
        if (message.getBusinessPartnerID().get(0).getVersions().get(0).getHasUnderwritingCodeID() != null &&
                message.getBusinessPartnerID().get(0).getVersions().get(0).getHasUnderwritingCodeID().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getVersions().get(0).getHasUnderwritingCodeID().get(0).getTECHSourceSystem().toString().equals(zepasConfig.getZEPAS())
        ) {
            return true;
        }
        if (message.getBusinessPartnerID().get(0).getVersions().get(0).getIsDuplicate() != null &&
                message.getBusinessPartnerID().get(0).getVersions().get(0).getIsDuplicate().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getVersions().get(0).getIsDuplicate().get(0).getTECHSourceSystem().toString().equals(zepasConfig.getZEPAS())
        ) {
            return true;
        }
        String logTransactionId = LogHelper.getTechMdmPartnerID(message.getBusinessPartnerID().get(0))
                + "|" + LogHelper.getPartnerId(message.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                + "|" + LogHelper.getPartnerId(message.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);

        logHelper.logInfo("", logTransactionId, "message is not relevant");
        return false;
    }
}
