package com.helvetia.app.mdm.streams;


import ch.mdm.zepas.dlq.DLQMessages;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.app.mdm.streams.exception.dlq.DLQException;
import com.helvetia.app.mdm.streams.model.ResponseOrDlqmessages;
import com.helvetia.app.mdm.streams.model.ResultOrExceptionWithInput;
import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import com.helvetia.app.mdm.streams.model.UnlockMessage;
import com.helvetia.app.mdm.streams.processor.*;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.util.Collections;
import java.util.Map;

// found it a bit unclear which properties are passed to StreamsConfig. See here for the relevant prefixes
// https://github.com/quarkusio/quarkus/blob/224fd01898801e9c15d2e0db73c32af4332a1d24/extensions/kafka-streams/runtime/src/main/java/io/quarkus/kafka/streams/runtime/KafkaStreamsPropertiesUtil.java#L18
// everything with prefix kafka-streams is passed to StreamsConfiguration

@ApplicationScoped
@Slf4j
public class TopologyProducer {

    @Inject
    MapperAndSender<readFullPartnerAllVersionsFromDateResponse, Boolean> mapperAndSender;

    @Inject
    RelevantMessageFilter<readFullPartnerAllVersionsFromDateResponse> relevantMessageFilter;

    @Inject
    RelationToEntity<readFullPartnerAllVersionsFromDateResponse, TechMDMPartnerID> relationToEntity;

    @Inject
    LogHelper logHelper;

    @ConfigProperty(name = "ch.helvetia.kafka.input.topic")
    String inputTopicName;

    @ConfigProperty(name = "ch.helvetia.kafka.unlock.topic")
    String unlockTopicName;

    @ConfigProperty(name = "ch.helvetia.kafka.dlq.topic")
    String dlqTopicName;

    @ConfigProperty(name = "quarkus.kafka-streams.schema-registry-url")
    String schemaRegistryUrl;

    @Inject
    LogAsUnrelatableProcessor logAsUnrelatableProcessor;

    @Inject
    LogIncomingProcessor logIncomingProcessor;

    @Inject
    MapToDeadLetterQueueTransformer mapToDeadLetterQueueTransformer;
    @Inject
    IsPartnerLockedTransformer isPartnerLockedTransformer;
    @Inject
    UnlockAndMaybeReprocessDlqProcessor unlockAndMaybeReprocessDlqProcessor;

    public static final String DLQ_STORE_NAME = "dlqStore";

    @Produces
    public Topology buildTopology() {

        // see https://github.com/confluentinc/kafka-streams-examples/blob/5.5.1-post/src/test/java/io/confluent/examples/streams/SpecificAvroIntegrationTest.java
        // When you want to override serdes explicitly/selectively
        final Map<String, String> serdeConfig = Collections.singletonMap("schema.registry.url",
                schemaRegistryUrl);
        final Serde<readFullPartnerAllVersionsFromDateResponse> responseAvroSerde = new SpecificAvroSerde<>();
        responseAvroSerde.configure(serdeConfig, false); // `false` for record values

        final Serde<DLQMessages> dlqMessagesAvroSerde = new SpecificAvroSerde<>();
        dlqMessagesAvroSerde.configure(serdeConfig, false); // `false` for record values

        StreamsBuilder builder = new StreamsBuilder();

        final GlobalKTable<TechMDMPartnerID, DLQMessages> dlqGlobalKTable = builder.globalTable(dlqTopicName,
                Consumed.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde),
                Materialized.as(
                        DLQ_STORE_NAME /* table/store name */));

        /** Unlock messages arrive ... **/

        final KStream<String, UnlockMessage> unlockStream = builder.stream(unlockTopicName, Consumed.with(Serdes.String(), UnlockMessage.serde));
        unlockStream
                .transform(() -> unlockAndMaybeReprocessDlqProcessor) // returns deletes after unlock, or nothing if nothing there
                .to(dlqTopicName, Produced.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde));

        /** Regular messages arrive ... **/

        final KStream<String, readFullPartnerAllVersionsFromDateResponse> mdmInputStream = builder.stream(inputTopicName, Consumed.with(Serdes.String(), responseAvroSerde));

        mdmInputStream.process(() -> logIncomingProcessor);

        @SuppressWarnings("unchecked") final KStream<String, readFullPartnerAllVersionsFromDateResponse>[] branch = mdmInputStream.branch(
                (key, response) -> relationToEntity.canBeRelatedToEntity(response),
                (key, response) -> !relationToEntity.canBeRelatedToEntity(response)
        );
        final KStream<String, readFullPartnerAllVersionsFromDateResponse> mdmValidUncheckedInputStream = branch[0];
        final KStream<String, readFullPartnerAllVersionsFromDateResponse> mdmUnrelatableInputStream = branch[1];

        mdmUnrelatableInputStream.process(() -> logAsUnrelatableProcessor);

        @SuppressWarnings("unchecked")
        KStream<TechMDMPartnerID, ResponseOrDlqmessages>[] maybeLockedInputStream = mdmValidUncheckedInputStream
                .selectKey((k, v) -> relationToEntity.extractEntity(v))
                .transform(() -> isPartnerLockedTransformer)
                .branch((k, v) -> v.isResponse(),
                        (k, v) -> v.isDlqMessages());

        maybeLockedInputStream[1]
                .mapValues(ResponseOrDlqmessages::getDlqMessages)
                .to(dlqTopicName, Produced.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde));

        final KStream<TechMDMPartnerID, readFullPartnerAllVersionsFromDateResponse> zepasRelevantMessages = maybeLockedInputStream[0]
                .mapValues(ResponseOrDlqmessages::getResponse)
                .filter((k, v) -> relevantMessageFilter.isMessageRelevant(v));


        @SuppressWarnings("unchecked") final KStream<TechMDMPartnerID, ResultOrExceptionWithInput<readFullPartnerAllVersionsFromDateResponse, Boolean>>[] successesOrExceptions =
                zepasRelevantMessages
                        .mapValues(mapperAndSender::mapAndSend)
                        .branch((k, v) -> v.isResult(), (k, v) -> !v.isResult());

        final KStream<TechMDMPartnerID, ResultOrExceptionWithInput<readFullPartnerAllVersionsFromDateResponse, Boolean>> failures = successesOrExceptions[1];

        failures
                .peek((k, v) -> throwJobStoppingExceptions(v))
                .transform(() -> mapToDeadLetterQueueTransformer)
                .to(dlqTopicName, Produced.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde));

        return builder.build();
    }


    private void throwJobStoppingExceptions(ResultOrExceptionWithInput errorResult) {
        final Exception exception = errorResult.getException();
        if (shouldNotThrow(exception)) {
            return;
        }
        if (exception instanceof RuntimeException) {
            throw (RuntimeException) exception;
        } else {
            throw new RuntimeException(exception);
        }
    }

    private boolean shouldNotThrow(Exception exception) {
        return exception instanceof DLQException;
    }


}
