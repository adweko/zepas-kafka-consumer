package com.helvetia.app.mdm.streams;

public interface RelationToEntity<M, E> {
    boolean canBeRelatedToEntity(M message);
    E extractEntity(M message);

}
