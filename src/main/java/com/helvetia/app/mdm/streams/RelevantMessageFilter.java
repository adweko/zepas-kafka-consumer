package com.helvetia.app.mdm.streams;

@FunctionalInterface
public interface RelevantMessageFilter<M> {
    boolean isMessageRelevant(M message);
}
