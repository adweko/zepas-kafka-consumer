package com.helvetia.app.mdm.streams.model;


import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class AvroSchemaFilesCreator {

    public static final String LOCATION = "src/main/resources/avro/";


    public static void main(String[] args) {

        try (PrintWriter out = new PrintWriter(LOCATION + DLQMessage.class.getSimpleName() + ".avsc")) {
            out.write(DLQMessage.schema.toString(true));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        try (PrintWriter out = new PrintWriter(LOCATION + DLQMessages.class.getSimpleName() + ".avsc")) {
            out.write(DLQMessages.schema.toString(true));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    private static final class DLQMessage {
        public static final String SOURCE_TOPIC = "sourceTopic";
        public static final String SOURCE_PARTITION = "sourcePartition";
        public static final String SOURCE_OFFSET = "sourceOffset";
        public static final String SOURCE_TIMESTAMP = "sourceTimestamp";
        public static final String ORIGINAL_MESSAGE = "originalMessage";
        public static final String RECORD_NAMESPACE = "ch.mdm.zepas.dlq";
        public static final String RECORD_NAME = "DLQMessage";
        public static final Schema schema = SchemaBuilder.record(RECORD_NAME).namespace(DLQMessage.RECORD_NAMESPACE)
                .fields()
                .requiredString(SOURCE_TOPIC)
                .requiredInt(SOURCE_PARTITION)
                .requiredLong(SOURCE_OFFSET)
                .requiredLong(SOURCE_TIMESTAMP)
                .name(ORIGINAL_MESSAGE).type(readFullPartnerAllVersionsFromDateResponse.getClassSchema()).noDefault()
                .endRecord();
    }

    private static final class DLQMessages {
        public static final String MESSAGES = "messages";
        public static final String RECORD_NAME = "DLQMessages";
        public static final String RECORD_NAMESPACE = "ch.mdm.zepas.dlq";
        public static final Schema schema = SchemaBuilder.record(RECORD_NAME).namespace(RECORD_NAMESPACE)
                .fields()
                .name(MESSAGES).type().array().items(DLQMessage.schema).noDefault()
                .endRecord();
    }


}
