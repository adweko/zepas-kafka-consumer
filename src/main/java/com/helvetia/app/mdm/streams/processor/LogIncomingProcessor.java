package com.helvetia.app.mdm.streams.processor;

import com.helvetia.app.mdm.logging.LogHelper;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Need an extra Processor for this, since topic, partition and offset are not directly accessible in KStreams
 */
@ApplicationScoped
public class LogIncomingProcessor implements Processor<String, readFullPartnerAllVersionsFromDateResponse> {
    private ProcessorContext context;

    private ProcessorContext context() {
        return context;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        this.context = processorContext;
    }

    @Override
    public void close() {
    }

    @Inject
    LogHelper logHelper;

    @Override
    public void process(String key, readFullPartnerAllVersionsFromDateResponse value) {
        String logTransactionId = "";
        try {
            logTransactionId = LogHelper.getTechMdmPartnerID(value.getBusinessPartnerID().get(0))
                    + "|" + LogHelper.getPartnerId(value.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                    + "|" + LogHelper.getPartnerId(value.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);
        } catch (Exception e) {
            logHelper.logError("", "", "could not extract transaction id from message");
        }

        logHelper.logInfo("", logTransactionId, context, "message received.");
    }
}