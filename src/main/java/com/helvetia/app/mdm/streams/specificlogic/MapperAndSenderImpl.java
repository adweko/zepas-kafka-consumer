package com.helvetia.app.mdm.streams.specificlogic;


import com.helvetia.app.mdm.config.HelperWsClient;
import com.helvetia.app.mdm.config.WsConfiguration;
import com.helvetia.app.mdm.config.ZepasConfig;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.app.mdm.mapping.LegalPersonMapper;
import com.helvetia.app.mdm.mapping.NaturalPersonMapper;
import com.helvetia.app.mdm.streams.MapperAndSender;
import com.helvetia.app.mdm.streams.exception.dlq.MappingException;
import com.helvetia.app.mdm.streams.exception.dlq.SOAPException;
import com.helvetia.app.mdm.streams.exception.dlq.ZepasDataQualityException;
import com.helvetia.app.mdm.streams.model.ResultOrExceptionWithInput;
import generated.MdmFullPerson;
import generated.MdmFullPersonV50;
import generated.Status;
import generated.ZepaswebServiceException_Exception;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.ws.soap.SOAPFaultException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class MapperAndSenderImpl implements MapperAndSender<readFullPartnerAllVersionsFromDateResponse, Boolean> {
    @Inject
    ZepasConfig zepasConfig;

    @Inject
    WsConfiguration wsConfiguration;

    @Inject
    LogHelper logHelper;

    @Inject
    NaturalPersonMapper naturalPersonMapper;

    @Inject
    LegalPersonMapper legalPersonMapper;

    MdmFullPersonV50 port = null;



    @Override
    public ResultOrExceptionWithInput<readFullPartnerAllVersionsFromDateResponse, Boolean> mapAndSend(readFullPartnerAllVersionsFromDateResponse v) {
        String logTransactionId = "";
        try {
            logTransactionId = LogHelper.getTechMdmPartnerID(v.getBusinessPartnerID().get(0))
                    + "|" + LogHelper.getPartnerId(v.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                    + "|" + LogHelper.getPartnerId(v.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);
        } catch (Exception e) {
            logHelper.logError("", "", "could not extract transaction id from message");
        }

        try {
            if (port == null) {
                port = zepasConfig.getService().getPort(zepasConfig.getPORT_NAME(), MdmFullPersonV50.class);
                wsConfiguration.setup();
                zepasConfig.getWsControl().setUserId(wsConfiguration.getLoggedInUser());
                zepasConfig.getWsControl().setLanguage("de");
            }



            logHelper.logInfo(zepasConfig.getWsControl().getUserId(), logTransactionId, "value: " + v);
            MdmFullPerson mdmFullPerson;

            try {
                HelperWsClient.prepareWs(port, wsConfiguration.getWsUrlMdmFullPerson(), wsConfiguration);
                mdmFullPerson = mapPartnerForZepas(v);
            } catch (Exception exception) {
                throw new MappingException();
            }

            port.synchronize(mdmFullPerson, HelperWsClient.getCalendar(Date.from(Instant.now())), zepasConfig.getWsControl(),
                    zepasConfig.getPersonenLaufNummer(), zepasConfig.getStatus());
//            for (Status status: zepasConfig.getStatus().value){
//                if (!status.getReturnCode().equals("200")){
//                    throw new ZepasDataQualityException();
//                }
//            }
            return new ResultOrExceptionWithInput<>(true);
        } catch (SOAPFaultException exception) {
            logHelper.logError(exception.getFault().getFaultString(), exception);
            Exception classifiedException = classifyException(exception, logTransactionId);
            return new ResultOrExceptionWithInput<>(v, classifiedException);
        } catch (Exception exception) {
            logHelper.logError(logTransactionId, exception);
            Exception classifiedException = classifyException(exception, logTransactionId);
            return new ResultOrExceptionWithInput<>(v, classifiedException);
        }
    }

    /**
     * Rethrow as DLQException if appropriate
     */
    private Exception classifyException(Exception e, String trId) {
        // wrap here any known DQ exceptions into subclasses of DLQException
        if ( e instanceof ZepaswebServiceException_Exception) {
            logHelper.logError("", trId, "Classifying " + ZepaswebServiceException_Exception.class.getSimpleName() + " as a Zepas data quality exception. Will go to DLQ.");
            return new ZepasDataQualityException(e);
        }
        if ( e instanceof SOAPFaultException) {
            logHelper.logError("", trId, "Classifying " + SOAPFaultException.class.getSimpleName() + " as a SOAP exception. Will go to DLQ.");
            return new SOAPException(e);
        }
        return e;
    }


    private MdmFullPerson mapPartnerForZepas(readFullPartnerAllVersionsFromDateResponse v) throws Exception {

        String sourceSytem = String.valueOf(v.getBusinessPartnerID().get(0).getTECHSourceSystem());
        List<VersionsEntry> versions = v.getBusinessPartnerID().get(0).getVersions();
        VersionsEntry version = versions.get(0);
        MdmFullPerson mdmFullPerson = null;
        if (version.getIsNaturalPerson().size() > 0 && !sourceSytem.equals(zepasConfig.getZEPAS())) {
            mdmFullPerson = naturalPersonMapper.mapMdmFullPerson(v);
        } else if (version.getIsLegalPerson().size() > 0 && !sourceSytem.equals(zepasConfig.getZEPAS())) {
            if (!version.getHasChannelID().isEmpty()) { // Syrius sends legalPersons always without addresses first
                mdmFullPerson = legalPersonMapper.mapMdmFullPerson(version);
            }
        }
        return mdmFullPerson;
    }
}

