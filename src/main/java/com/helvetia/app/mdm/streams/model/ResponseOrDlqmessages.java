package com.helvetia.app.mdm.streams.model;


import ch.mdm.zepas.dlq.DLQMessages;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

public class ResponseOrDlqmessages {
    private final DLQMessages dlqMessages;
    private final readFullPartnerAllVersionsFromDateResponse response;

    public ResponseOrDlqmessages(DLQMessages dlqMessages) {
        this.dlqMessages = dlqMessages;
        this.response = null;
    }
    public ResponseOrDlqmessages(readFullPartnerAllVersionsFromDateResponse response) {
        this.response = response;
        this.dlqMessages = null;
    }

    public boolean isResponse() {
        return response != null;
    }

    public boolean isDlqMessages() {
        return dlqMessages != null;
    }

    public DLQMessages getDlqMessages() {
        return dlqMessages;
    }

    public readFullPartnerAllVersionsFromDateResponse getResponse() {
        return response;
    }
}
