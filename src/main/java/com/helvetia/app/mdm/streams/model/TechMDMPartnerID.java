package com.helvetia.app.mdm.streams.model;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Objects;

public class TechMDMPartnerID {

    public static final Serde<TechMDMPartnerID> serde = new Serde<>() {
        @Override
        public Serializer<TechMDMPartnerID> serializer() {
            return ((topic, data) -> Serdes.String().serializer().serialize(topic, data.getValue()));
        }

        @Override
        public Deserializer<TechMDMPartnerID> deserializer() {
            return ((topic, data) -> {
                final String deserialized = Serdes.String().deserializer().deserialize(topic, data);
                return new TechMDMPartnerID(deserialized);
            });
        }
    };

    private final String value;

    public TechMDMPartnerID(String techMDMPartnerId) {
        this.value = techMDMPartnerId;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "TechMDMPartnerID{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechMDMPartnerID that = (TechMDMPartnerID) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
