package com.helvetia.app.mdm.streams.processor;


import ch.mdm.zepas.dlq.DLQMessage;
import ch.mdm.zepas.dlq.DLQMessages;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.app.mdm.streams.model.ResultOrExceptionWithInput;
import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Collections;

@ApplicationScoped
public class MapToDeadLetterQueueTransformer<R> implements Transformer<TechMDMPartnerID, ResultOrExceptionWithInput<readFullPartnerAllVersionsFromDateResponse, R>, KeyValue<TechMDMPartnerID, DLQMessages>> {

    private ProcessorContext context;

    @Inject
    LogHelper logHelper;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public KeyValue<TechMDMPartnerID, DLQMessages> transform(TechMDMPartnerID techMDMPartnerID, ResultOrExceptionWithInput<readFullPartnerAllVersionsFromDateResponse, R> value) {
        DLQMessage dlqMessage = new DLQMessage(context.topic(), context.partition(), context.offset(), context.timestamp(), value.getInput());

        String logTransactionId = LogHelper.getTechMdmPartnerID(value.getInput().getBusinessPartnerID().get(0))
                + "|" + LogHelper.getPartnerId(value.getInput().getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                + "|" + LogHelper.getPartnerId(value.getInput().getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);
        logHelper.logError("unknown User", logTransactionId, context, "message send to DLQ");

        return new KeyValue<>(techMDMPartnerID, new DLQMessages(Collections.singletonList(dlqMessage)));

    }


    @Override
    public void close() {

    }
}
