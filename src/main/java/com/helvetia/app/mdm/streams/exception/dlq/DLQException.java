package com.helvetia.app.mdm.streams.exception.dlq;

public abstract class DLQException extends RuntimeException {
    public DLQException() {
        super();
    }

    public DLQException(String message) {
        super(message);
    }

    public DLQException(String message, Throwable cause) {
        super(message, cause);
    }

    public DLQException(Throwable cause) {
        super(cause);
    }
}
