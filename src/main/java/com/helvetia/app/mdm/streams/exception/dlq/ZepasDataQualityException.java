package com.helvetia.app.mdm.streams.exception.dlq;

public class ZepasDataQualityException extends DLQException {
    public ZepasDataQualityException() {
        super();
    }

    public ZepasDataQualityException(String message) {
        super(message);
    }

    public ZepasDataQualityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZepasDataQualityException(Throwable cause) {
        super(cause);
    }
}
