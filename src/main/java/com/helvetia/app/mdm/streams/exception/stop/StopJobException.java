package com.helvetia.app.mdm.streams.exception.stop;

public abstract class StopJobException extends RuntimeException {
    public StopJobException() {
        super();
    }

    public StopJobException(String message) {
        super(message);
    }

    public StopJobException(String message, Throwable cause) {
        super(message, cause);
    }

    public StopJobException(Throwable cause) {
        super(cause);
    }
}
