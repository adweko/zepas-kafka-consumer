package com.helvetia.app.mdm.streams.processor;


import ch.mdm.zepas.dlq.DLQMessage;
import ch.mdm.zepas.dlq.DLQMessages;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.app.mdm.streams.MapperAndSender;
import com.helvetia.app.mdm.streams.TopologyProducer;
import com.helvetia.app.mdm.streams.exception.stop.ReprocessingFailedException;
import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import com.helvetia.app.mdm.streams.model.UnlockMessage;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Instant;

@ApplicationScoped
public class UnlockAndMaybeReprocessDlqProcessor implements Transformer<String, UnlockMessage, KeyValue<TechMDMPartnerID, DLQMessages>> {
    private ProcessorContext context;
    private TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages> store;

    @Inject
    MapperAndSender<readFullPartnerAllVersionsFromDateResponse, Boolean> mapperAndSender;

    @Inject
    LogHelper logHelper;


    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
        store = (TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages>) context.getStateStore(TopologyProducer.DLQ_STORE_NAME);
    }

    @Override
    public KeyValue<TechMDMPartnerID, DLQMessages> transform(String s, UnlockMessage unlockMessage) {
        final TechMDMPartnerID partnerId = unlockMessage.getPartnerId();

        logHelper.logInfo("unknown user", null, context, "unlock message received " + unlockMessage.toJsonString());

        ValueAndTimestamp<DLQMessages> dlqMessagesValueAndTimestamp = store.get(partnerId);

        if (dlqMessagesValueAndTimestamp == null) {
            return null;
        }
        DLQMessages lockedMessages = dlqMessagesValueAndTimestamp.value();
        // reprocess all dlqueued messages after startingTimeStamp
        for (DLQMessage msg : lockedMessages.getMessages()) {
            Instant msgTimstamp = Instant.ofEpochMilli(msg.getSourceTimestamp());
            if (msgTimstamp.isAfter(unlockMessage.getRepublishedAt().getInstant())) {
                logHelper.logInfo("unknown user", null, context, "Reprocessing message from DLQ. Original message from: see offset");
                final readFullPartnerAllVersionsFromDateResponse originalMessage = msg.getOriginalMessage();
                try {
                    mapperAndSender.mapAndSend(originalMessage);
                } catch (Exception exception) {
                    final String errorMessage = "Processing failed with exception. Please check the exception. If " +
                            "it is of a technical, recoverable nature (like target system down) ensure to fix it in" +
                            " environment, then republish again. If it is data quality, correct the source data in MDM" +
                            " & republish again. The job will continue, but the partner will stay locked until the next republish.";
                    ReprocessingFailedException reprocessingFailedException = new ReprocessingFailedException(errorMessage, exception);
                    logHelper.logError("", null, errorMessage);
                    logHelper.logError(null, reprocessingFailedException);
                    return null;
                }

            }
        }
        // return delete message for DLQ
        return new KeyValue<>(partnerId, null);
    }


    @Override
    public void close() {

    }

}
