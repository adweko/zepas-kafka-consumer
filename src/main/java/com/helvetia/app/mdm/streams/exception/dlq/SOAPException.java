package com.helvetia.app.mdm.streams.exception.dlq;

public class SOAPException extends DLQException {
    public SOAPException() {
        super();
    }

    public SOAPException(String message) {
        super(message);
    }

    public SOAPException(String message, Throwable cause) {
        super(message, cause);
    }

    public SOAPException(Throwable cause) {
        super(cause);
    }
}
