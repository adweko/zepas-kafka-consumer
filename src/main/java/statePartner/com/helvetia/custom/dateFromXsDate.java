/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package statePartner.com.helvetia.custom;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class dateFromXsDate extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -669521677932146919L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"dateFromXsDate\",\"namespace\":\"statePartner.com.helvetia.custom\",\"fields\":[{\"name\":\"day\",\"type\":\"int\"},{\"name\":\"month\",\"type\":\"int\"},{\"name\":\"year\",\"type\":\"int\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<dateFromXsDate> ENCODER =
      new BinaryMessageEncoder<dateFromXsDate>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<dateFromXsDate> DECODER =
      new BinaryMessageDecoder<dateFromXsDate>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<dateFromXsDate> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<dateFromXsDate> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<dateFromXsDate> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<dateFromXsDate>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this dateFromXsDate to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a dateFromXsDate from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a dateFromXsDate instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static dateFromXsDate fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public int day;
  @Deprecated public int month;
  @Deprecated public int year;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public dateFromXsDate() {}

  /**
   * All-args constructor.
   * @param day The new value for day
   * @param month The new value for month
   * @param year The new value for year
   */
  public dateFromXsDate(java.lang.Integer day, java.lang.Integer month, java.lang.Integer year) {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return day;
    case 1: return month;
    case 2: return year;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: day = (java.lang.Integer)value$; break;
    case 1: month = (java.lang.Integer)value$; break;
    case 2: year = (java.lang.Integer)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'day' field.
   * @return The value of the 'day' field.
   */
  public int getDay() {
    return day;
  }


  /**
   * Sets the value of the 'day' field.
   * @param value the value to set.
   */
  public void setDay(int value) {
    this.day = value;
  }

  /**
   * Gets the value of the 'month' field.
   * @return The value of the 'month' field.
   */
  public int getMonth() {
    return month;
  }


  /**
   * Sets the value of the 'month' field.
   * @param value the value to set.
   */
  public void setMonth(int value) {
    this.month = value;
  }

  /**
   * Gets the value of the 'year' field.
   * @return The value of the 'year' field.
   */
  public int getYear() {
    return year;
  }


  /**
   * Sets the value of the 'year' field.
   * @param value the value to set.
   */
  public void setYear(int value) {
    this.year = value;
  }

  /**
   * Creates a new dateFromXsDate RecordBuilder.
   * @return A new dateFromXsDate RecordBuilder
   */
  public static statePartner.com.helvetia.custom.dateFromXsDate.Builder newBuilder() {
    return new statePartner.com.helvetia.custom.dateFromXsDate.Builder();
  }

  /**
   * Creates a new dateFromXsDate RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new dateFromXsDate RecordBuilder
   */
  public static statePartner.com.helvetia.custom.dateFromXsDate.Builder newBuilder(statePartner.com.helvetia.custom.dateFromXsDate.Builder other) {
    if (other == null) {
      return new statePartner.com.helvetia.custom.dateFromXsDate.Builder();
    } else {
      return new statePartner.com.helvetia.custom.dateFromXsDate.Builder(other);
    }
  }

  /**
   * Creates a new dateFromXsDate RecordBuilder by copying an existing dateFromXsDate instance.
   * @param other The existing instance to copy.
   * @return A new dateFromXsDate RecordBuilder
   */
  public static statePartner.com.helvetia.custom.dateFromXsDate.Builder newBuilder(statePartner.com.helvetia.custom.dateFromXsDate other) {
    if (other == null) {
      return new statePartner.com.helvetia.custom.dateFromXsDate.Builder();
    } else {
      return new statePartner.com.helvetia.custom.dateFromXsDate.Builder(other);
    }
  }

  /**
   * RecordBuilder for dateFromXsDate instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<dateFromXsDate>
    implements org.apache.avro.data.RecordBuilder<dateFromXsDate> {

    private int day;
    private int month;
    private int year;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(statePartner.com.helvetia.custom.dateFromXsDate.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.day)) {
        this.day = data().deepCopy(fields()[0].schema(), other.day);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.month)) {
        this.month = data().deepCopy(fields()[1].schema(), other.month);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.year)) {
        this.year = data().deepCopy(fields()[2].schema(), other.year);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
    }

    /**
     * Creates a Builder by copying an existing dateFromXsDate instance
     * @param other The existing instance to copy.
     */
    private Builder(statePartner.com.helvetia.custom.dateFromXsDate other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.day)) {
        this.day = data().deepCopy(fields()[0].schema(), other.day);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.month)) {
        this.month = data().deepCopy(fields()[1].schema(), other.month);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.year)) {
        this.year = data().deepCopy(fields()[2].schema(), other.year);
        fieldSetFlags()[2] = true;
      }
    }

    /**
      * Gets the value of the 'day' field.
      * @return The value.
      */
    public int getDay() {
      return day;
    }


    /**
      * Sets the value of the 'day' field.
      * @param value The value of 'day'.
      * @return This builder.
      */
    public statePartner.com.helvetia.custom.dateFromXsDate.Builder setDay(int value) {
      validate(fields()[0], value);
      this.day = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'day' field has been set.
      * @return True if the 'day' field has been set, false otherwise.
      */
    public boolean hasDay() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'day' field.
      * @return This builder.
      */
    public statePartner.com.helvetia.custom.dateFromXsDate.Builder clearDay() {
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'month' field.
      * @return The value.
      */
    public int getMonth() {
      return month;
    }


    /**
      * Sets the value of the 'month' field.
      * @param value The value of 'month'.
      * @return This builder.
      */
    public statePartner.com.helvetia.custom.dateFromXsDate.Builder setMonth(int value) {
      validate(fields()[1], value);
      this.month = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'month' field has been set.
      * @return True if the 'month' field has been set, false otherwise.
      */
    public boolean hasMonth() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'month' field.
      * @return This builder.
      */
    public statePartner.com.helvetia.custom.dateFromXsDate.Builder clearMonth() {
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'year' field.
      * @return The value.
      */
    public int getYear() {
      return year;
    }


    /**
      * Sets the value of the 'year' field.
      * @param value The value of 'year'.
      * @return This builder.
      */
    public statePartner.com.helvetia.custom.dateFromXsDate.Builder setYear(int value) {
      validate(fields()[2], value);
      this.year = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'year' field has been set.
      * @return True if the 'year' field has been set, false otherwise.
      */
    public boolean hasYear() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'year' field.
      * @return This builder.
      */
    public statePartner.com.helvetia.custom.dateFromXsDate.Builder clearYear() {
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public dateFromXsDate build() {
      try {
        dateFromXsDate record = new dateFromXsDate();
        record.day = fieldSetFlags()[0] ? this.day : (java.lang.Integer) defaultValue(fields()[0]);
        record.month = fieldSetFlags()[1] ? this.month : (java.lang.Integer) defaultValue(fields()[1]);
        record.year = fieldSetFlags()[2] ? this.year : (java.lang.Integer) defaultValue(fields()[2]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<dateFromXsDate>
    WRITER$ = (org.apache.avro.io.DatumWriter<dateFromXsDate>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<dateFromXsDate>
    READER$ = (org.apache.avro.io.DatumReader<dateFromXsDate>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeInt(this.day);

    out.writeInt(this.month);

    out.writeInt(this.year);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.day = in.readInt();

      this.month = in.readInt();

      this.year = in.readInt();

    } else {
      for (int i = 0; i < 3; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.day = in.readInt();
          break;

        case 1:
          this.month = in.readInt();
          break;

        case 2:
          this.year = in.readInt();
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










