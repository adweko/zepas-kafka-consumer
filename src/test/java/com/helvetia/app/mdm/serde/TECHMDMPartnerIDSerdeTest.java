package com.helvetia.app.mdm.serde;


import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TECHMDMPartnerIDSerdeTest {

    @Test
    public void roundTrip() {
        final TechMDMPartnerID techMDMPartnerID = new TechMDMPartnerID("123");
        final byte[] serialized = TechMDMPartnerID.serde.serializer().serialize("topic", techMDMPartnerID);
        final TechMDMPartnerID deserialized = TechMDMPartnerID.serde.deserializer().deserialize("topic", serialized);
        Assertions.assertEquals(techMDMPartnerID, deserialized);
    }
}
