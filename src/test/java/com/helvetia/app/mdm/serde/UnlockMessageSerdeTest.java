package com.helvetia.app.mdm.serde;

import com.helvetia.app.mdm.streams.model.UnlockMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UnlockMessageSerdeTest {

    @Test
    public void roundTrip() {
        String raw = "{\"TECHMDMPartnerID\":\"E107B98D-5083-436D-9513-D626B9C475BC\",\"RepublishedAt\":\"20200819152152623\"}";
        final UnlockMessage unlockMessage = UnlockMessage.fromJsonString(raw);
        Assertions.assertEquals("E107B98D-5083-436D-9513-D626B9C475BC", unlockMessage.getPartnerId().getValue());
    }

   

}
