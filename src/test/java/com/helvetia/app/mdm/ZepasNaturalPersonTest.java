/*
 * Copyright (C) 2018 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.mdm;

import com.helvetia.app.mdm.config.HelperWsClient;
import com.helvetia.app.mdm.config.MockWsConfiguration;
import com.helvetia.app.mdm.mapping.NaturalPersonMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import generated.*;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.avro.Schema;
import org.junit.jupiter.api.Test;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@QuarkusTest
public class ZepasNaturalPersonTest {

    @Inject
    NaturalPersonMapper naturalPersonMapper;

    @Inject
    MockWsConfiguration wsConfiguration;

    @Inject
    ZepasTestHelper zepasTestHelper;

    JsonAvroConverter converter = new JsonAvroConverter();

    MdmFullPersonV50_Service service = new MdmFullPersonV50_Service(
            ZepasDummyTest.class.getResource("/wsdl/MdmFullPerson_v5_0.wsdl"));
    static final QName PORT_NAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/",
            "MdmFullPerson_v5_0Port");
    MdmFullPersonV50 port;

    Holder<Long> personenLaufNummer;
    Holder<List<Status>> status;
    WsControl wsControl;

    // MFNLT-15887
    @Test
    public void testExample() throws Exception {
        createNatPersonWithActAddress("example_20200817.json", false);
    }

    // mit Adresse und vordatierter Adresse
    @Test
    public void testUpdateNatPersonWithAddressAndFuture() throws Exception {
        createNatPersonWithActAddress("create_with_act_and_future_address.json", false);
    }

    //ohne Adresse
    @Test
    public void testUpdateEingabesbNatPerson() throws Exception {
        createNatPersonWithActAddress("update_eingabesb.json", false);
    }

    @Test
    public void testCreateNatPerson() throws Exception {
        createNatPersonWithActAddress("create.json", true);
    }

    @Test
    public void testCreateNatPersonWithActAddress() throws Exception {
        createNatPersonWithActAddress("create_with_act_address.json", true);
    }

    @Test
    public void testCreateNatPersonWithActAddressAndPostfach() throws Exception {
        createNatPersonWithActAddress("create_with_act_address_postfach.json", true);
    }

    @Test
    public void testCreateNatPersonWithActAddressAndFuture() throws Exception {
        createNatPersonWithActAddress("create_with_act_and_future_address.json", true);
    }

    @Test
    public void testCreateNatPersonWithFutureAddress() throws Exception {
        createNatPersonWithActAddress("create_with_future_address.json", true);
    }

    @Test
    public void testCreateWithActAddressAndUwCode() throws Exception {
        createNatPersonWithActAddress("create_with_act_address_and_uw_code.json", true);
    }

    @Test
    public void testKommkanal() throws Exception {
        createNatPersonWithActAddress("kommkanal.json", true);
    }

    private void createNatPersonWithActAddress(String jsonFile, boolean cleanup) throws Exception {
        port = service.getPort(PORT_NAME, MdmFullPersonV50.class);
        wsConfiguration.setup();
        HelperWsClient
                .prepareWs(port, wsConfiguration.getWsUrlMdmFullPerson(),
                        wsConfiguration);
        personenLaufNummer = new Holder<>();
        status = new Holder<>();
        wsControl = new WsControl();
        wsControl.setUserId(wsConfiguration.getLoggedInUser());
        wsControl.setLanguage("de");
        MdmFullPerson mdmFullPerson = null;
        Path avsc = Paths.get("src/main/resources/avro/state.avsc");
        File example = new File(
                ZepasNaturalPersonTest.class.getClassLoader().getResource("NatPerson/" + jsonFile).getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        readFullPartnerAllVersionsFromDateResponse stateMessage = converter
                .convertToSpecificRecord(context.jsonString().getBytes(),
                        readFullPartnerAllVersionsFromDateResponse.class, schema);
            mdmFullPerson = naturalPersonMapper
                    .mapMdmFullPerson(stateMessage);
            port.synchronize(mdmFullPerson, HelperWsClient.getCalendar(Date.from(Instant.now())), wsControl,
                    personenLaufNummer, status);
            HelperWsClient.printStatus(status.value);
        if (cleanup) {
            zepasTestHelper.cleanupPerson(mdmFullPerson, port, wsControl);
        }
    }
}