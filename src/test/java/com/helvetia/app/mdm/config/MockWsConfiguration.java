package com.helvetia.app.mdm.config;

import io.quarkus.test.Mock;
import lombok.Data;
import org.apache.commons.io.IOUtils;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;

@Mock
@Data
@ApplicationScoped
public class MockWsConfiguration extends WsConfiguration{

//    @ConfigProperty(name = "ch.helvetia.zepas.endpoint")
//    String wsUrlMdmFullPerson;

//    String loggedInUser;
//    byte[] samltokenSignerKeystore;
//    String samltokenSignerPassword;


    @Override
    public void setup() throws IOException {
        loggedInUser = "CHTI-MDM";
        samltokenSignerKeystore = IOUtils.toByteArray(WsConfiguration.class.getResourceAsStream("/security/TKSIGN_DEVL_MDM.jks"));
        samltokenSignerPassword = "hib1t3juM79Jy6Vk21el";
    }

}
