package com.helvetia.app.mdm.streams;


import ch.mdm.zepas.dlq.DLQMessages;
import com.helvetia.app.mdm.streams.exception.dlq.MappingException;
import com.helvetia.app.mdm.streams.exception.stop.SendingFailedException;
import com.helvetia.app.mdm.streams.model.MDMTimestamp;
import com.helvetia.app.mdm.streams.model.ResultOrExceptionWithInput;
import com.helvetia.app.mdm.streams.model.TechMDMPartnerID;
import com.helvetia.app.mdm.streams.model.UnlockMessage;
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueStore;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import statePartner.json.Response.Row;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isBusinessPartner.isBusinessPartnerEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isNaturalPerson.isNaturalPersonEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertThrows;

@QuarkusTest
public class TopologyTest {

    @ConfigProperty(name = "ch.helvetia.kafka.input.topic")
    String inputTopicName;

    @ConfigProperty(name = "ch.helvetia.kafka.unlock.topic")
    String unlockTopicName;

    @Inject
    Topology topology;

    @InjectSpy
    MockMapperAndSenderImpl mapperAndSender;

    TopologyTestDriver testDriver;

    private static final String SCHEMA_REGISTRY_SCOPE = "schemaregmock";
    private static final String MOCK_SCHEMA_REGISTRY_URL = "mock://" + SCHEMA_REGISTRY_SCOPE;

    TestInputTopic<String, readFullPartnerAllVersionsFromDateResponse> stateTopic;
    TestInputTopic<String, String> chMDmUnlockTopic;

    KeyValueStore<TechMDMPartnerID, DLQMessages> dlqStore;

    @AfterEach
    public void tearDown() {
        testDriver.getAllStateStores().values().forEach(StateStore::flush);
        testDriver.close();
        MockSchemaRegistry.dropScope(SCHEMA_REGISTRY_SCOPE);
    }


    @BeforeEach
    public void setUp() {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "testApplicationId");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        config.put("schema.registry.url", MOCK_SCHEMA_REGISTRY_URL);
        testDriver = new TopologyTestDriver(topology, config);

        Serde<readFullPartnerAllVersionsFromDateResponse> avroChangeMdmResponseSerde = new SpecificAvroSerde<>();

        // Configure Serdes to use the same mock schema registry URL
        Map<String, String> serdeConfig = Map.of(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, MOCK_SCHEMA_REGISTRY_URL);
        avroChangeMdmResponseSerde.configure(serdeConfig, false);

        stateTopic = testDriver.createInputTopic(inputTopicName, new StringSerializer(), avroChangeMdmResponseSerde.serializer());
        chMDmUnlockTopic = testDriver.createInputTopic(unlockTopicName, new StringSerializer(), new StringSerializer());

        dlqStore = testDriver.getKeyValueStore(TopologyProducer.DLQ_STORE_NAME);

    }


    @Test
    public void regularProcessing() {
        final readFullPartnerAllVersionsFromDateResponse response = buildValidNatPerson();
        stateTopic.pipeInput(response);
        Assertions.assertEquals(0, dlqStore.approximateNumEntries());
    }

    @Test
    public void messageWithMissingFieldsGoesToDLQ() {
        String partnerId = "123";
        readFullPartnerAllVersionsFromDateResponse inputMsg = buildRelevantMinimalNatPerson(partnerId);
        stateTopic.pipeInput(inputMsg);
        final DLQMessages dlqMessage = dlqStore.get(new TechMDMPartnerID(partnerId));
        Assertions.assertNotNull(dlqMessage);
        System.out.println(dlqMessage);
        Assertions.assertEquals(inputMsg, dlqMessage.getMessages().get(0).getOriginalMessage());
    }

    @Test
    public void unlockMessageClearsDlqsOldRecords() {
        String partnerId = "123";
        final TechMDMPartnerID techMDMPartnerID = new TechMDMPartnerID(partnerId);
        readFullPartnerAllVersionsFromDateResponse inputMsg = buildRelevantMinimalNatPerson(partnerId);
        Instant t1 = Instant.now();
        stateTopic.pipeInput(inputMsg, t1);
        final DLQMessages dlqMessage = dlqStore.get(new TechMDMPartnerID(partnerId));
        Assertions.assertEquals(inputMsg, dlqMessage.getMessages().get(0).getOriginalMessage());

        Instant t2 = t1.plusSeconds(10);
        final UnlockMessage unlockMessage = UnlockMessage.create(techMDMPartnerID, new MDMTimestamp(t2));
        chMDmUnlockTopic.pipeInput(unlockMessage.toJsonString());
        Assertions.assertEquals(0, dlqStore.approximateNumEntries());
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
    }

    @Test
    public void mappingErrorsGoToDLQ() {
        readFullPartnerAllVersionsFromDateResponse response = buildValidNatPerson();
        final TechMDMPartnerID techMDMPartnerID = new TechMDMPartnerID(response.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString());
        // make invalid
        response.getBusinessPartnerID().get(0).getVersions().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("INVALID_TIMESTAMP");
        stateTopic.pipeInput(response);
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
        DLQMessages dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(response, dlqMessage.getMessages().get(0).getOriginalMessage());
    }

    @Test
    public void validMessagesReceivedTooEarlyInDlqAreReprocessed() {
        Instant t1 = Instant.now();
        Instant t2 = t1.plusSeconds(10);
        Instant t3 = t2.plusSeconds(10);
        readFullPartnerAllVersionsFromDateResponse invalidMessage = buildValidNatPerson();
        final TechMDMPartnerID techMDMPartnerID = new TechMDMPartnerID(invalidMessage.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString());
        // make invalid
        invalidMessage.getBusinessPartnerID().get(0).getVersions().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("INVALID_TIMESTAMP");
        stateTopic.pipeInput(invalidMessage, t1);
        // called once, but threw MappingException
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
        // went to DLQ
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
        DLQMessages dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(invalidMessage, dlqMessage.getMessages().get(0).getOriginalMessage());

        // now pipe a valid msg for same Partner at t3, simulate arrival prior to unlock at t2. Should got to DLQ as well
        readFullPartnerAllVersionsFromDateResponse validMessage = buildValidNatPerson();
        stateTopic.pipeInput(validMessage, t3);

        dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(validMessage, dlqMessage.getMessages().get(1).getOriginalMessage());

        final UnlockMessage unlockMessage = UnlockMessage.create(techMDMPartnerID, new MDMTimestamp(t2));
        chMDmUnlockTopic.pipeInput(unlockMessage.toJsonString());

        // now map and send was called once more for valid partner
        Mockito.verify(mapperAndSender, Mockito.times(2)).mapAndSend(Mockito.any());
        // and dlq is empty
        Assertions.assertNull(dlqStore.get(techMDMPartnerID));

    }

    @Test
    public void errorDuringReprocessingDlqCanBeHandledByRepublishingPartnerAgain() {
        Instant t1 = Instant.now();
        Instant t2 = t1.plusSeconds(10);
        Instant t3 = t2.plusSeconds(10);
        readFullPartnerAllVersionsFromDateResponse invalidMessage = buildValidNatPerson();
        final TechMDMPartnerID techMDMPartnerID = new TechMDMPartnerID(invalidMessage.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString());
        // make invalid
        invalidMessage.getBusinessPartnerID().get(0).getVersions().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("INVALID_TIMESTAMP");
        stateTopic.pipeInput(invalidMessage, t1);
        // called once, but threw MappingException
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
        // went to DLQ
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
        DLQMessages dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(invalidMessage, dlqMessage.getMessages().get(0).getOriginalMessage());

        // now pipe another invalid msg for same Partner at t3, simulate arrival prior to unlock at t2. Should go to DLQ as well
        readFullPartnerAllVersionsFromDateResponse secondInvalidMessage = buildValidNatPerson();
        secondInvalidMessage.getBusinessPartnerID().get(0).getVersions().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("SECOND_sINVALID_TIMESTAMP");
        stateTopic.pipeInput(secondInvalidMessage, t3);

        dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(secondInvalidMessage, dlqMessage.getMessages().get(1).getOriginalMessage());

        // make mock return failure

        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenThrow(new SendingFailedException("mock sending failure"));

        final UnlockMessage unlockMessage = UnlockMessage.create(techMDMPartnerID, new MDMTimestamp(t2));

        chMDmUnlockTopic.pipeInput(unlockMessage.toJsonString());
        // now map and send was called once more for the second invalid partner
        Mockito.verify(mapperAndSender, Mockito.times(2)).mapAndSend(Mockito.any());
        // the dlq is still full
        Assertions.assertTrue(dlqStore.get(techMDMPartnerID).getMessages().size() == 2);

        //sending another unlock message at t4 will clear the dlq
        Instant t4 = t3.plusSeconds(10);
        final UnlockMessage unlockMessageT4 = UnlockMessage.create(techMDMPartnerID, new MDMTimestamp(t4));

        chMDmUnlockTopic.pipeInput(unlockMessageT4.toJsonString());
        // now dlq is clear
        Assertions.assertNull(dlqStore.get(techMDMPartnerID));

    }

    @Test
    public void unknownErrorsLeadToCrash() {
        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenReturn(
                new ResultOrExceptionWithInput<>(null, new Exception("something"))
        );
        Exception exception = assertThrows(Exception.class, () -> {
            readFullPartnerAllVersionsFromDateResponse inputMessage = buildValidNatPerson();
            stateTopic.pipeInput(inputMessage);
        });
    }

    @Test
    public void SendingFailedLeadsToCrash() {
        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenReturn(
                new ResultOrExceptionWithInput<>(null, new SendingFailedException("send failed"))
        );
        Exception exception = assertThrows(Exception.class, () -> {
            readFullPartnerAllVersionsFromDateResponse inputMessage = buildValidNatPerson();
            stateTopic.pipeInput(inputMessage);
        });
        Assert.assertTrue(exception instanceof SendingFailedException);
    }

    @Test
    public void MappingFailedGoesToDLQ() {
        readFullPartnerAllVersionsFromDateResponse inputMessage = buildValidNatPerson();
        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenReturn(
                new ResultOrExceptionWithInput<>(inputMessage, new MappingException("mapping failed"))
        );
        stateTopic.pipeInput(inputMessage);
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
    }


    private static readFullPartnerAllVersionsFromDateResponse buildRelevantMinimalNatPerson(String partnerId) {
        return readFullPartnerAllVersionsFromDateResponse.newBuilder().setBusinessPartnerID(Collections.singletonList(
                Row.newBuilder()
                        .setTECHMDMPartnerID(partnerId)
                        .setVersions(Collections.singletonList(
                                VersionsEntry.newBuilder()
                                        .setIsBusinessPartner(Collections.singletonList(
                                                isBusinessPartnerEntry.newBuilder()
                                                .setTECHSourceSystem("NOT_ZEPAS")
                                                .build()
                                        ))
                                        .setIsNaturalPerson(Collections.singletonList(
                                                isNaturalPersonEntry.newBuilder()
                                                        .setTECHMDMPartnerID(partnerId)
                                                        .build()
                                        )).build()

                        )).build()
        )).build();
    }


    private static readFullPartnerAllVersionsFromDateResponse buildValidNatPerson() {
        final String msg = "{\"BusinessPartnerID\": [{\"BusinessPartnerType\": \"NaturalPerson\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"Versions\": [{\"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"_stp_id\": \"IdentificationNumber:185000941\", \"_stp_label\": \"185000941\", \"_stp_type\": \"IdentificationNumber\", \"IDNumber\": \"85000941\", \"IDType\": \"1\", \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\"}], \"_stp_key\": \"20200616144830965\", \"_stp_label\": \"hasIdentificationNumber\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144830965\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_id\": \"hasIdentificationNumber:20200616144830965\"}, {\"IdentificationNumber\": [{\"_stp_id\": \"IdentificationNumber:2f5037b52-9e71-4002-9097-afc5445f945d\", \"_stp_label\": \"2f5037b52-9e71-4002-9097-afc5445f945d\", \"_stp_type\": \"IdentificationNumber\", \"IDNumber\": \"f5037b52-9e71-4002-9097-afc5445f945d\", \"IDType\": \"2\", \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\"}], \"_stp_key\": \"20200616144830965\", \"_stp_label\": \"hasIdentificationNumber\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144830965\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_id\": \"hasIdentificationNumber:20200616144830965\"}], \"isBusinessPartner\": [{\"BusinessPartner\": [{\"_stp_id\": \"BusinessPartner:39079EB33BBC3A8D402DAD53EB34E279\", \"_stp_label\": \"39079EB33BBC3A8D402DAD53EB34E279\", \"_stp_type\": \"BusinessPartner\", \"encryptionCode_VIP\": null, \"newBusinessPartnerID\": null, \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null}], \"_stp_id\": \"isBusinessPartner:20200616144853905\", \"_stp_key\": \"20200616144853905\", \"_stp_label\": \"isBusinessPartner\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144853905\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null}], \"isLegalPerson\": [], \"isNaturalPerson\": [{\"NaturalPerson\": [{\"_stp_id\": \"NaturalPerson:1DB425C0B68AEEE5E8BD7551CA5138B2\", \"_stp_label\": \"1DB425C0B68AEEE5E8BD7551CA5138B2\", \"_stp_type\": \"NaturalPerson\", \"academicDegree\": null, \"alienStatus\": null, \"creditRating\": null, \"dateOfBirth\": {\"day\": 1, \"month\": 1, \"year\": 1960}, \"dateOfDeath\": {\"day\": 1, \"month\": 1, \"year\": 2020}, \"discountCategory\": null, \"DNA\": null, \"driverLicenseID\": null, \"dutyToContribute\": null, \"employmentGrade\": null, \"employmentStatus\": null, \"function_value\": null, \"goalAndNeed\": null, \"healthData\": null, \"healthInformation\": null, \"hobby\": null, \"homeTown\": \"Z?rich\", \"inEducation\": null, \"isBiometric\": null, \"language\": \"D\", \"maritalStatus\": \"02\", \"membership\": null, \"name\": \"Test\", \"nameAtBirth\": \"Test\", \"nationalIDCard\": null, \"nationality\": \"CH\", \"noAds\": null, \"note\": null, \"passport\": null, \"pets\": null, \"placeOfBirth\": null, \"religion\": null, \"residencePermit\": null, \"residencePermitValidTo\": null, \"salutation\": null, \"secondName\": null, \"securityClassification\": null, \"sex\": \"1\", \"signature\": null, \"skills\": null, \"socialAssistanceRecipients\": null, \"socialSecurityNumber\": \"756.1234.5678.97\", \"stopCode\": null, \"surname\": \"N?chster\", \"thirdName\": null, \"VIP\": null, \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourcePartnerID\": null, \"TECHSourceSystem\": \"SYRIUS_MF\", \"domicileCity\": null, \"domicileCountry\": null, \"domicileHouseNumber\": null, \"domicileRegion\": null, \"domicileStreet\": null, \"domicileZIP\": null, \"employee\": null, \"otherTitle\": \"Titel\", \"TECHUser\": null}], \"_stp_id\": \"isNaturalPerson:20200616144830965\", \"_stp_key\": \"20200616144830965\", \"_stp_label\": \"isNaturalPerson\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144830965\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"0\", \"TECHInternalPID\": \"6C1DF2A8-6202-488C-A799-0986F1BFC581\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null}], \"Version\": \"1\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"hasChannelID\": [{\"ChannelID\": [{\"_stp_id\": \"ChannelID:714A2A2C61828AEA52DFA9EB5B139CBA\", \"_stp_label\": \"714A2A2C61828AEA52DFA9EB5B139CBA\", \"_stp_type\": \"ChannelID\", \"channelID\": \"736E0B63-DEEE-4605-B1F5-60D7A06AA604\", \"channelType\": \"3\", \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"hasTelephone\": [{\"Telephone\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"telephoneNumber\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasSkype\": [{\"Skype\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"skypeAddress\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"isChannel\": [{\"Channel\": [{\"_stp_id\": \"Channel:A69888D23A06287A0A1F917B0C57CBFF\", \"_stp_label\": \"A69888D23A06287A0A1F917B0C57CBFF\", \"_stp_type\": \"Channel\", \"channelNote\": \"121\", \"noAds\": null, \"preferredLanguage\": null, \"privateBusiness\": \"G\", \"routing\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"usage\": null}], \"_stp_id\": \"isChannel\", \"_stp_key\": null, \"_stp_label\": \"isChannel\", \"BITEMPDeleted\": null, \"BITEMPInvalid\": null, \"BITEMPMDMTimestamp\": \"20200616144853905\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasMobile\": [{\"Mobile\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"mobileNumber\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"hasEmail\": [{\"Email\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"emailAddress\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasFax\": [{\"Fax\": [{\"_stp_id\": \"Fax:+41 71 923 65 65\", \"_stp_label\": \"+41 71 923 65 65\", \"_stp_type\": \"Fax\", \"faxNumber\": \"+41 71 923 65 65\", \"TECHExternalPID\": \"fead6290-93e6-4ea4-b078-5368a4f9906b\", \"TECHInternalPID\": \"3F79F5FC-A422-4CEC-9EF7-C7B15045DF7D\", \"TECHSourceSystem\": \"SYRIUS_MF\"}], \"_stp_id\": \"hasFax\", \"_stp_key\": null, \"_stp_label\": \"hasFax\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144853905\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": null}], \"hasIdentificationNumber\": [{\"_stp_id\": \"hasIdentificationNumber:F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_key\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_label\": \"hasIdentificationNumber\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144853905\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"IdentificationNumber\": [{\"_stp_id\": \"IdentificationNumber:1KUKO200000292\", \"_stp_label\": \"1KUKO200000292\", \"_stp_type\": \"IdentificationNumber\", \"IDNumber\": \"200000292\", \"IDType\": \"1KUKO\", \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}]}], \"hasLocationID\": [], \"hasSite\": [{\"Site\": [{\"flat\": null, \"floor\": null, \"geoCoordinates\": null, \"locationDescription\": null}], \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}]}], \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"D6E45B33-D62A-4303-885E-724015A3155F\", \"BITEMPMDMTimestamp\": null}, {\"ChannelID\": [{\"_stp_id\": \"ChannelID:8523ACFF539E21AEB42BB8A821101B35\", \"_stp_label\": \"8523ACFF539E21AEB42BB8A821101B35\", \"_stp_type\": \"ChannelID\", \"channelID\": \"27c4d08c-9647-4935-b1cd-7cf20cf42229\", \"channelType\": \"6\", \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"hasTelephone\": [{\"Telephone\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"telephoneNumber\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasSkype\": [{\"Skype\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"skypeAddress\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"isChannel\": [{\"Channel\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"channelNote\": null, \"noAds\": null, \"preferredLanguage\": null, \"privateBusiness\": null, \"routing\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"usage\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPInvalid\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasMobile\": [{\"Mobile\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"mobileNumber\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"hasEmail\": [{\"Email\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"emailAddress\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasFax\": [{\"Fax\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"faxNumber\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasIdentificationNumber\": [{\"_stp_id\": \"hasIdentificationNumber:F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_key\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_label\": \"hasIdentificationNumber\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144836644\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"IdentificationNumber\": [{\"_stp_id\": \"IdentificationNumber:1ADDR200001782\", \"_stp_label\": \"1ADDR200001782\", \"_stp_type\": \"IdentificationNumber\", \"IDNumber\": \"200001782\", \"IDType\": \"1ADDR\", \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}]}], \"hasLocationID\": [{\"_stp_id\": \"hasLocationID\", \"_stp_key\": null, \"_stp_label\": \"hasLocationID\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144836644\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"LocationID\": [{\"_stp_id\": \"LocationID:41AE0508442A37A14EB8F481E4D28884\", \"_stp_label\": \"41AE0508442A37A14EB8F481E4D28884\", \"_stp_type\": \"LocationID\", \"locationID\": \"8c465ab0-18ec-4441-9154-036789075add\", \"newLocationID\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"usage\": null, \"hasRegion\": [{\"_stp_id\": \"hasRegion\", \"_stp_key\": null, \"_stp_label\": \"hasRegion\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144836644\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"Region\": [{\"_stp_id\": \"Region:90F079871B436CD24905F3BA26542D08\", \"_stp_label\": \"90F079871B436CD24905F3BA26542D08\", \"_stp_type\": \"Region\", \"regionName\": \"ZH\", \"regionType\": null, \"TECHExternalPID\": \"2c042378-98d8-46b6-8423-7898d8c6b63f\", \"TECHInternalPID\": \"108FD0CC-FBDD-4102-BCCE-78361AFB8892\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHUser\": \"CHE0MZA\"}]}], \"hasAddressLocationID\": [{\"_stp_id\": \"hasLocationID\", \"_stp_key\": null, \"_stp_label\": \"hasLocationID\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144836644\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"AddressLocationID\": [{\"_stp_id\": \"AddressLocationID:BF55553F-7095-425E-A19E-194AF5C8B3CF\", \"_stp_label\": \"BF55553F-7095-425E-A19E-194AF5C8B3CF\", \"_stp_type\": \"AddressLocationID\", \"addressLocationID\": \"BF55553F-7095-425E-A19E-194AF5C8B3CF\", \"TECHExternalPID\": \"a5c868db-479e-42cb-8868-db479ea2cb86\", \"TECHInternalPID\": \"1FE82514-F990-46C1-973A-9CB302F8743F\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHUser\": \"CHE0MZA\", \"hasAddressLocation\": [{\"_stp_id\": \"hasAddressLocation\", \"_stp_key\": null, \"_stp_label\": \"hasAddressLocation\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616112707748\", \"BITEMPSourceTimestamp\": \"20200616132659387\", \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"a5c868db-479e-42cb-8868-db479ea2cb86\", \"TECHInternalPID\": \"1FE82514-F990-46C1-973A-9CB302F8743F\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHUser\": \"CHE0MZA\", \"AddressLocation\": [{\"_stp_id\": \"AddressLocation:834E910F489176CCB8D9EFD69A2F5A4D\", \"_stp_label\": \"834E910F489176CCB8D9EFD69A2F5A4D\", \"_stp_type\": \"AddressLocation\", \"addressID\": \"0ca7fcab-f826-4f01-91b4-94f8ac716332\", \"addressReference\": null, \"addressRegion\": null, \"city\": \"Z?rich\", \"country\": \"CH\", \"houseNumber\": \"354\", \"postbox\": null, \"street\": \"Hofwiesenstrasse\", \"TECHExternalPID\": \"981cf87a-d3ea-4d19-9cf8-7ad3eafd1992\", \"TECHInternalPID\": \"947395D4-4A60-487F-A5F1-396E257B8A15\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": \"CHTI-MDM\", \"ZIPCode\": \"8050\", \"ZIPCodeExtension\": \"00\"}]}]}]}]}]}], \"hasSite\": [{\"Site\": [{\"flat\": \"1. OG\", \"floor\": null, \"geoCoordinates\": null, \"locationDescription\": \"Blaues Haus\"}], \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144836644\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null}]}], \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": \"629EF3D7-69C8-43DA-8481-4DB0FBDCC601\", \"BITEMPMDMTimestamp\": null}, {\"ChannelID\": [{\"_stp_id\": \"ChannelID:BD3CE75FDF095E4E46974E946B632667\", \"_stp_label\": \"BD3CE75FDF095E4E46974E946B632667\", \"_stp_type\": \"ChannelID\", \"channelID\": \"53F84A1B-2868-4310-A211-1DC65AB453B5\", \"channelType\": \"4\", \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"BB471326-9D47-45FD-8F60-667864591AEE\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"hasTelephone\": [{\"Telephone\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"telephoneNumber\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasSkype\": [{\"Skype\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"skypeAddress\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"isChannel\": [{\"Channel\": [{\"_stp_id\": \"Channel:411CFB89E2A51E78DC6735B5C66271DC\", \"_stp_label\": \"411CFB89E2A51E78DC6735B5C66271DC\", \"_stp_type\": \"Channel\", \"channelNote\": \"130\", \"noAds\": null, \"preferredLanguage\": null, \"privateBusiness\": \"P\", \"routing\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": \"BB471326-9D47-45FD-8F60-667864591AEE\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"usage\": null}], \"_stp_id\": \"isChannel\", \"_stp_key\": null, \"_stp_label\": \"isChannel\", \"BITEMPDeleted\": null, \"BITEMPInvalid\": null, \"BITEMPMDMTimestamp\": \"20200616144842675\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasMobile\": [{\"Mobile\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"mobileNumber\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"hasEmail\": [{\"Email\": [{\"_stp_id\": \"EMail:email@privat.ch\", \"_stp_label\": \"email@privat.ch\", \"_stp_type\": \"EMail\", \"emailAddress\": \"email@privat.ch\", \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"e0991405-427b-4648-a492-9ab8a97a5ef8\", \"TECHSourceSystem\": \"CH_ZEPAS_IL\"}], \"_stp_id\": \"hasEMail\", \"_stp_key\": null, \"_stp_label\": \"hasEMail\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144842675\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"BB471326-9D47-45FD-8F60-667864591AEE\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": \"SYRIUS_MF\"}], \"hasFax\": [{\"Fax\": [{\"_stp_id\": null, \"_stp_label\": null, \"_stp_type\": null, \"faxNumber\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null}], \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null, \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null}], \"hasIdentificationNumber\": [{\"_stp_id\": \"hasIdentificationNumber:F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_key\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"_stp_label\": \"hasIdentificationNumber\", \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200616144842675\", \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"BB471326-9D47-45FD-8F60-667864591AEE\", \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"IdentificationNumber\": [{\"_stp_id\": \"IdentificationNumber:1KUKO200000291\", \"_stp_label\": \"1KUKO200000291\", \"_stp_type\": \"IdentificationNumber\", \"IDNumber\": \"200000291\", \"IDType\": \"1KUKO\", \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"BB471326-9D47-45FD-8F60-667864591AEE\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}]}], \"hasLocationID\": [], \"hasSite\": [{\"Site\": [{\"flat\": null, \"floor\": null, \"geoCoordinates\": null, \"locationDescription\": null}], \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": null, \"BITEMPSourceTimestamp\": null, \"BITEMPValidFrom\": null, \"BITEMPValidTo\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}]}], \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"TECHUser\": null, \"TECHExternalPID\": \"256df0ad-e49b-435d-8f52-3f0365026f56\", \"TECHInternalPID\": \"BB471326-9D47-45FD-8F60-667864591AEE\", \"BITEMPMDMTimestamp\": null}], \"hasUnderwritingCode\": null, \"BITEMPValidFrom\": {\"day\": 16, \"month\": 6, \"year\": 2020}}], \"keyDateComp\": {\"day\": 16, \"month\": 6, \"year\": 2020}, \"TECHMDMPartnerID\": \"F5037B52-9E71-4002-9097-AFC5445F945D\", \"readMsg\": null, \"CRUD\": \"UPDATE\", \"TECHInternalPIDIn\": null, \"TECHSourceSystemIn\": null, \"Status\": null, \"Status_Code\": null, \"Status_Description\": null, \"user_fields\": []}]}";
        JsonAvroConverter converter = new JsonAvroConverter();
        return converter.convertToSpecificRecord(msg.getBytes(), readFullPartnerAllVersionsFromDateResponse.class, readFullPartnerAllVersionsFromDateResponse.getClassSchema());
    }
}
