/*
 * Copyright (C) 2020 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.mdm;

import generated.MdmFullPerson;
import generated.MdmFullPersonV50;
import generated.WsControl;
import generated.ZepaswebServiceException_Exception;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class ZepasTestHelper {

    protected void cleanupPerson(MdmFullPerson mdmFullPerson, MdmFullPersonV50 port, WsControl wsControl)
            throws ZepaswebServiceException_Exception {
        if (mdmFullPerson != null && mdmFullPerson.getMdmCompositePerson() != null) {
            Long personenLaufNummer = mdmFullPerson.getMdmCompositePerson().getMdmPersonenLaufnummer();
            List<Long> deletedRows = port.cleanup(personenLaufNummer, wsControl);
        }
    }
}
