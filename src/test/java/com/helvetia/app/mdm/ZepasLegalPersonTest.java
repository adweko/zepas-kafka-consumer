/*
 * Copyright (C) 2018 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.mdm;

import com.helvetia.app.mdm.config.HelperWsClient;
import com.helvetia.app.mdm.config.MockWsConfiguration;
import com.helvetia.app.mdm.mapping.LegalPersonMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import generated.*;
import io.quarkus.test.junit.QuarkusTest;

import org.apache.avro.Schema;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@QuarkusTest
public class ZepasLegalPersonTest {

    @Inject
    LegalPersonMapper legalPersonMapper;

    @Inject
    MockWsConfiguration wsConfiguration;

    @Inject
    ZepasTestHelper zepasTestHelper;

    JsonAvroConverter converter = new JsonAvroConverter();


    MdmFullPersonV50_Service service = new MdmFullPersonV50_Service(
            ZepasDummyTest.class.getResource("/wsdl/MdmFullPerson_v5_0.wsdl"));
    static final QName PORT_NAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/",
            "MdmFullPerson_v5_0Port");
    MdmFullPersonV50 port;

    Holder<Long> personenLaufNummer;
    Holder<List<Status>> status;
    WsControl wsControl;

    @Test
    public void LegalPerson55010471WithChannel() throws Exception {
        createLegalPersonWithActAddress("55010471_with_channel.json", true);
    }

    @Test
    public void LegalPerson55010471WithoutChannel() throws Exception {
        createLegalPersonWithActAddress("55010471_without_channel.json", true);
    }

    @Test
    public void testCreateLegalPersonWithActAddress() throws Exception {
        createLegalPersonWithActAddress("legalPerson_with_act_address.json", true);
    }

    @Disabled
    @Test
    public void testCreateLegalPersonWithActAddressAndPostfach() throws Exception {
        createLegalPersonWithActAddress("create_with_act_address_postfach.json", true);
    }

    @Disabled
    @Test
    public void testCreateLegalPersonWithActAddressAndFuture() throws Exception {
        createLegalPersonWithActAddress("create_with_act_and_future_address.json", true);
    }

    private void createLegalPersonWithActAddress(String jsonFile, boolean cleanup) throws Exception {
        port = service.getPort(PORT_NAME, MdmFullPersonV50.class);
        wsConfiguration.setup();
        HelperWsClient
                .prepareWs(port, wsConfiguration.getWsUrlMdmFullPerson(),
                        wsConfiguration);
        personenLaufNummer = new Holder<>();
        status = new Holder<>();
        wsControl = new WsControl();
        wsControl.setUserId(wsConfiguration.getLoggedInUser());
        wsControl.setLanguage("de");
        MdmFullPerson mdmFullPerson = null;
        Path avsc = Paths.get("src/main/resources/avro/state.avsc");
        File example = new File(
                ZepasLegalPersonTest.class.getClassLoader().getResource("LegPerson/" + jsonFile).getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        readFullPartnerAllVersionsFromDateResponse stateMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), readFullPartnerAllVersionsFromDateResponse.class, schema);
        List<VersionsEntry> versions = stateMessage.getBusinessPartnerID().get(0).getVersions();
        for (int i = 0; i < versions.size() && i <= 1; i++) {
            VersionsEntry version = versions.get(i);
            if ( version.getHasChannelID().isEmpty() ) {
                System.out.println("createLegalPersonWithActAddress ("+jsonFile+"): Ignoring legal Person without channel");
            } else {
                System.out.println("createLegalPersonWithActAddress ("+jsonFile+"): Processing legal Person with channel");
                mdmFullPerson = legalPersonMapper
                        .mapMdmFullPerson(version);
            }
            port.synchronize(mdmFullPerson, HelperWsClient.getCalendar(Date.from(Instant.now())), wsControl,
                    personenLaufNummer, status);
            HelperWsClient.printStatus(status.value);
        }
        if (cleanup) {
            zepasTestHelper.cleanupPerson(mdmFullPerson, port, wsControl);
        }
    }
}