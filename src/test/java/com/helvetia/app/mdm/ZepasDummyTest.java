package com.helvetia.app.mdm;

import com.helvetia.app.mdm.config.HelperWsClient;
import com.helvetia.app.mdm.config.MockWsConfiguration;
import com.helvetia.app.mdm.logging.LogHelper;
import com.helvetia.heap.util.DateHelper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import generated.*;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@QuarkusTest
public class ZepasDummyTest {

    @Inject
    MockWsConfiguration wsConfiguration;
    @Inject
    ZepasTestHelper zepasTestHelper;
    @Inject
    LogHelper logHelper;

    Configuration conf = Configuration.builder().options(Option.DEFAULT_PATH_LEAF_TO_NULL).build();
    MdmFullPersonV50_Service service = new MdmFullPersonV50_Service(
            ZepasDummyTest.class.getResource("/wsdl/MdmFullPerson_v5_0.wsdl"));
    static final QName PORT_NAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/",
            "MdmFullPerson_v5_0Port");
    MdmFullPersonV50 port;
    Holder<Long> personenLaufNummer;
    Holder<List<Status>> status;
    WsControl wsControl;

    @Test
    public void testDummyLegalPersonAndAddress() throws Exception {

        long startTime = System.currentTimeMillis();
        port = service.getPort(PORT_NAME, MdmFullPersonV50.class);
        wsConfiguration.setup();

        String logTransactionId = UUID.randomUUID().toString();
        String logLoggedInUser = wsConfiguration.getLoggedInUser();

        HelperWsClient
                .prepareWs(port, wsConfiguration.getWsUrlMdmFullPerson(),
                        wsConfiguration);
        personenLaufNummer = new Holder<>();
        status = new Holder<>();
        wsControl = new WsControl();
        wsControl.setUserId(wsConfiguration.getLoggedInUser());
        wsControl.setLanguage("de");
        MdmFullPerson mdmFullPerson = getMdmFullPersonLegal();
        mdmFullPerson.getMdmAdressen().add(getMdmAddress());
        mdmFullPerson.getMdmKommunikationselemente().add(getMdmKommunikationselement());
        port.synchronize(mdmFullPerson, HelperWsClient.getCalendar(Date.from(Instant.now())), wsControl,
                personenLaufNummer, status);
        HelperWsClient.printStatus(status.value);
        logHelper.logInfo(logLoggedInUser, logTransactionId, "testDummyLegalPersonAndAddress() - before cleanup() person");
        zepasTestHelper.cleanupPerson(mdmFullPerson, port, wsControl);
    }

    private MdmFullPerson getMdmFullPersonLegal() throws Exception {
        wsConfiguration.setup();
        MdmFullPerson mdmFullPerson = new MdmFullPerson();
        MdmUnternehmung mdmUnternehmung = new MdmUnternehmung();
        MdmCompositePerson mdmCompositePerson = new MdmCompositePerson();
        //mdmUnternehmung.setAufloesungsdatum();
        mdmUnternehmung.setNogaCode("999999");
        mdmUnternehmung.setRechtsformCode("99");
        mdmUnternehmung.setUnternehmensId("CHE102242543");
        //        mdmUnternehmung.setGegenparteiId("");
        //        mdmUnternehmung.setMehrwertssteuernummer("");
        //        mdmUnternehmung.setStiftungstext1("");
        //        mdmUnternehmung.setStiftungstext2("");
        //        mdmUnternehmung.setAnredeCode("");
        mdmUnternehmung.setEingabeAm(HelperWsClient.formatLocalDate(DateHelper.localDateNow()));
        mdmUnternehmung.setEingabeSb("CHTI-MDM");
        mdmUnternehmung.setGueltigAb(HelperWsClient.formatLocalDate(DateHelper.localDateNow()));
        mdmUnternehmung.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmUnternehmung.setKennzeichenCode("U");
        mdmUnternehmung.setLaufnummer(0);
        mdmUnternehmung.setName("Testfirma");
        mdmUnternehmung.setNamenZusatz1("");
        mdmUnternehmung.setNamenZusatz2("");
        //        mdmUnternehmung.setRabattklasseCode();
        mdmUnternehmung.setSprachCode("D");
        mdmUnternehmung.setLaufnummer(99999990);
        mdmCompositePerson.setMdmUnternehmung(mdmUnternehmung);
        mdmCompositePerson.setMdmPersonenLaufnummer(mdmUnternehmung.getLaufnummer());
        mdmFullPerson.setMdmCompositePerson(mdmCompositePerson);
        return mdmFullPerson;
    }

    private MdmAdresse getMdmAddress() throws Exception {
        wsConfiguration.setup();
        MdmAdresse mdmAdresse = new MdmAdresse();
        mdmAdresse.setEingabeAm(HelperWsClient.formatLocalDate(DateHelper.localDateNow()));
        mdmAdresse.setEingabeSb("CHTI-MDM");
        mdmAdresse.setGueltigAb(HelperWsClient.formatLocalDate(DateHelper.localDateNow()));
        mdmAdresse.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmAdresse.setLaufnummer(0);
        mdmAdresse.setAdressartCode("001");
        //mdmAdresse.setLokalitaetsbezeichnung();
        //mdmAdresse.setWohnungsbezeichnung();
        //mdmAdresse.setPostfachnummer();
        mdmAdresse.setStrassenname("Sonnenbergstrasse");
        mdmAdresse.setHausnummer("12");
        mdmAdresse.setPostleitzahl("9000");
        mdmAdresse.setOrtsname("St. Gallen");
        //mdmAdresse.setRegion();
        mdmAdresse.setLandkennzeichenCode("CH");
        return mdmAdresse;
    }

    private MdmKommunikationselement getMdmKommunikationselement() throws Exception {
        wsConfiguration.setup();
        MdmKommunikationselement mdmKommunikationselement = new MdmKommunikationselement();
        mdmKommunikationselement.setEingabeAm(HelperWsClient.formatLocalDate(DateHelper.localDateNow()));
        mdmKommunikationselement.setEingabeSb("CHTI-MDM");
        mdmKommunikationselement.setGueltigAb(HelperWsClient.formatLocalDate(DateHelper.localDateNow()));
        mdmKommunikationselement.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmKommunikationselement.setLaufnummer(0);
        mdmKommunikationselement.setElementtypCode("130");
        mdmKommunikationselement.setKommentar("test");
        mdmKommunikationselement.setKommkanalCode("MAIL");
        mdmKommunikationselement.setPrioritaet(0);
        mdmKommunikationselement.setValue("test@gmail.com");
        return mdmKommunikationselement;
    }
}
